//
//  RPChangeAddressTo.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/13/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LMAddress.h"

@protocol RPChangeAddressTo <NSObject>
-(void)passAddressTo:(LMAddress*)address;
@end
