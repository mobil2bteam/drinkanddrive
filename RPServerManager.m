//
//  RPServerManager.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RPServerManager.h"
#import "AFNetworking.h"
#import "RPUser.h"
#import "RPCar.h"
#import "RPOptions.h"
#import "RPAddress.h"
#import "RPOrder.h"
#import "RPDriver.h"
#import "RPOrderRefuse.h"

#define kAddressSite @"http://www.dndmobil.ru/"

@interface RPServerManager ()
@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;
@end


@implementation RPServerManager : NSObject


+ (RPServerManager*) sharedManager {
    static RPServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPServerManager alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)initWithUrl:(NSURL*)url{
    self.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    self.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    self.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
}


- (void) getRootCategoriesWithParameters:(NSDictionary*)param
                               onSuccess:(void(^)(RPUser *user, NSString *errorMessage)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    [self.requestOperationManager POST:@"" parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
        if ([data valueForKeyPath:@"message.msg"]!=nil) {
            errorMessage = [data valueForKeyPath:@"message.msg"];
        } else {
            NSDictionary *userInfo = [data valueForKey:@"user_info"];
            [RPUser sharedUser].happymonth = [[userInfo valueForKey:@"happymonth"]integerValue];
            [RPUser sharedUser].happyday = [[userInfo valueForKey:@"happyday"]integerValue];
            [RPUser sharedUser].ball = [[userInfo valueForKey:@"ball"]integerValue];
            [RPUser sharedUser].active = [[userInfo valueForKey:@"active"]integerValue];
            [RPUser sharedUser].phone = [userInfo valueForKey:@"phone"];
            [RPUser sharedUser].name = [userInfo valueForKey:@"name"];
            [RPUser sharedUser].password = [userInfo valueForKey:@"password"];
            [RPUser sharedUser].userid = [[userInfo valueForKey:@"id"]integerValue];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[RPUser sharedUser].password forKey:@"password"];
            [userDefaults setObject:[RPUser sharedUser].phone forKey:@"phone"];
            [userDefaults synchronize];
            if ([userInfo valueForKey:@"message"]!=nil) {
                [RPUser sharedUser].message = [userInfo valueForKey:@"message"];
            }
        }
        if (success) {
            success(nil,errorMessage);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,404);
        }
    }];
}

- (void) getConfigsOnSuccess:(void(^)(RPOptions *options)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@mobindex.php?",kAddressSite]];
    [self initWithUrl:url];
    
    [self.requestOperationManager POST:@"" parameters:@{@"type":@"getoptions"}
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  [RPOptions sharedOptions].operatorPhone = [data valueForKey:@"operator"];
                                  [RPOptions sharedOptions].user_rates = [data valueForKey:@"user_rates"];
                                  [RPOptions sharedOptions].user_agreement = [data valueForKey:@"user_agreement"];
                                  [RPOptions sharedOptions].about_phone = [data valueForKey:@"about_phone"];
                                  [RPOptions sharedOptions].about_suite = [data valueForKey:@"about_suite"];
                                  [RPOptions sharedOptions].url_suite = [data valueForKey:@"url_suite"];
                                  [RPOptions sharedOptions].count_ball = [[data valueForKey:@"count_ball"]integerValue];
                                  [RPOptions sharedOptions].max_ball = [[data valueForKey:@"max_ball"]integerValue];

                                  
                                  [RPOptions sharedOptions].typecar = [[NSMutableArray alloc]init];
                                  for (NSDictionary *dic in [data valueForKey:@"typecar"]) {
                                      NSInteger typeId = [[dic valueForKey:@"id"]integerValue];
                                      NSString *name = [dic valueForKey:@"name"];
                                      NSDictionary *car = @{@"id":@(typeId),
                                                            @"name":name};
                                      [[RPOptions sharedOptions].typecar addObject:car];
                                  }
                                  [RPOptions sharedOptions].type_user_cancel = [[NSMutableArray alloc]init];
                                  for (NSDictionary *dic in [data valueForKey:@"type_user_cancel"]) {
                                      RPOrderRefuse *orderRefuse = [[RPOrderRefuse alloc]init];
                                      orderRefuse.ID = [[dic valueForKey:@"id"]integerValue];
                                      orderRefuse.name = [dic valueForKey:@"name"];
                                      orderRefuse.active = [[dic valueForKey:@"active"]integerValue];
                                      [[RPOptions sharedOptions].type_user_cancel addObject:orderRefuse];
                                  }

                                  if (success) {
                                      success(nil);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) getCarsWithParameters:(NSDictionary*)param
                     onSuccess:(void(^)(NSMutableArray <RPCar*> *cars, NSString *errorMessage)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    __block NSMutableArray <RPCar*> *cars = nil;
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data valueForKeyPath:@"message.msg"]!=nil) {
                                      errorMessage = [data valueForKeyPath:@"message.msg"];
                                  } else {
                                      cars = [[NSMutableArray <RPCar*> alloc]init];
                                      for (NSDictionary *dic in [data valueForKey:@"car_list"]) {
                                          RPCar *car = [[RPCar alloc]init];
                                          car.def = [[dic valueForKey:@"def"]integerValue];
                                          car.carId = [[dic valueForKey:@"id"]integerValue];
                                          car.userId = [[dic valueForKey:@"users_id"]integerValue];
                                          car.typeCar = [[dic valueForKey:@"type_car"]integerValue];
                                          car.marka = [dic valueForKey:@"marka"];
                                          car.numberCar = [dic valueForKey:@"number_car"];
                                          [cars addObject:car];
                                      }
                                  }
                                  
                                  if (success) {
                                      success(cars,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) getCarWithParameters:(NSDictionary*)param
                    onSuccess:(void(^)(RPCar *car, NSString *errorMessage)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    __block RPCar *car = nil;
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data valueForKeyPath:@"message.msg"]!=nil) {
                                      errorMessage = [data valueForKeyPath:@"message.msg"];
                                  } else {
                                      car = [[RPCar alloc]init];
                                      car.def = [[data valueForKeyPath:@"car_info.default"]integerValue];
                                      car.carId = [[data valueForKeyPath:@"car_info.id"]integerValue];
                                      car.typeCar = [[data valueForKeyPath:@"car_info.type_car"]integerValue];
                                      car.marka = [data valueForKeyPath:@"car_info.marka"];
                                      car.numberCar = [data valueForKeyPath:@"car_info.number"];
                                  }
                                  
                                  if (success) {
                                      success(car,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) removeCarWithParameters:(NSDictionary*)param
                       onSuccess:(void(^)(NSString *errorMessage)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data valueForKeyPath:@"message.msg"]!=nil) {
                                      errorMessage = [data valueForKeyPath:@"message.msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) getAddressesWithParameters:(NSDictionary*)param
                          onSuccess:(void(^)(NSMutableArray <RPAddress*> *addresses, NSString *errorMessage)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    __block NSMutableArray <RPAddress*> *addresses = nil;
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data valueForKeyPath:@"message.msg"]!=nil) {
                                      errorMessage = [data valueForKeyPath:@"message.msg"];
                                  } else {
                                      addresses = [[NSMutableArray <RPAddress*> alloc]init];
                                      for (NSDictionary *dic in [data valueForKey:@"address_list"]) {
                                          RPAddress *address = [[RPAddress alloc]init];
                                          address.city = [dic valueForKey:@"city"];
                                          address.addressId = [[dic valueForKey:@"id"]integerValue];
                                          address.korpus = [[dic valueForKey:@"korpus"]integerValue];
                                          address.stroenie = [[dic valueForKey:@"stroenie"]integerValue];
                                          address.street = [dic valueForKey:@"street"];
                                          address.house = [dic valueForKey:@"house"];
                                          address.comment = [dic valueForKey:@"comment"];
                                          [addresses addObject:address];
                                      }
                                  }
                                  if (success) {
                                      success(addresses,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) getAddressWithParameters:(NSDictionary*)param
                        onSuccess:(void(^)(RPAddress *address, NSString *errorMessage)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
    __block RPAddress *address = nil;    
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data valueForKeyPath:@"message.msg"]!=nil) {
                                      errorMessage = [data valueForKeyPath:@"message.msg"];
                                  } else {
                                      address = [[RPAddress alloc]init];
                                      NSDictionary *dic = [data valueForKey:@"address_info"];
                                      address.city = [dic valueForKey:@"city"];
                                      address.addressId = [[dic valueForKey:@"id"]integerValue];
                                      address.korpus = [[dic valueForKey:@"korpus"]integerValue];
                                      address.stroenie = [[dic valueForKey:@"stroenie"]integerValue];
                                      address.street = [dic valueForKey:@"street"];
                                      address.house = [dic valueForKey:@"house"];
                                      address.comment = [dic valueForKey:@"comment"];
                                      
                                  }
                                  
                                  if (success) {
                                      success(address,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
    
}


- (void) newOrder:(NSDictionary*)param
        onSuccess:(void(^)(RPOrder *order, NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block RPOrder *order = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      order = [[RPOrder alloc]init];
                                      [RPOrder sharedOrder].numberOrder = [[data valueForKey:@"number_order"]integerValue];
                                      [RPOrder sharedOrder].status = [[data valueForKey:@"status"]integerValue];
                                      [RPOrder sharedOrder].statusText = [data valueForKey:@"status_text"];
                                      [RPOrder sharedOrder].token = [data valueForKey:@"token"];
                                  }
                                  if (success) {
                                      success(order, nil);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) getOrder:(NSDictionary*)param
        onSuccess:(void(^)(RPOrder *order, NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block RPOrder *order = nil;
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  order = [[RPOrder alloc]init];
                                  [self setSigletonOrderWithData:data];
                                  
                                  if (success) {
                                      success(order, errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) sendMessageForDriver:(NSDictionary*)param
                    onSuccess:(void(^)(NSString *errorMessage)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  [self setSigletonOrderWithData:data];
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) checkOrder:(NSDictionary*)param
          onSuccess:(void(^)(NSString *token, NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block NSString *token = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  if ([data valueForKey:@"token"]!=nil) {
                                      token = [data valueForKey:@"token"];
                                  }
                                  if (success) {
                                      success(token,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) inviteFriend:(NSDictionary*)param
            onSuccess:(void(^)(NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) cancelOrder:(NSDictionary*)param
           onSuccess:(void(^)(NSString *errorMessage)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) closeOrderWithOcenka:(NSDictionary*)param
                    onSuccess:(void(^)(NSString *errorMessage)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) getOrderList:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block NSMutableArray <RPOrder*> *orderList = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      orderList = [[NSMutableArray <RPOrder*> alloc]init];
                                      NSArray *array = [data valueForKey:@"order_list"];
                                      for (NSDictionary *dic in array) {
                                          RPOrder *order = [[RPOrder alloc]init];
                                          order.adddressA = dic[@"address_a"];
                                          order.adddressB = dic[@"address_b"];
                                          order.driver = [[RPDriver alloc]init];
                                          order.driver.foto =  dic[@"driver"][@"foto"];
                                          order.driver.name =  dic[@"driver"][@"name"];
                                          order.driver.phone =  dic[@"driver"][@"phone"];
                                          order.driver.reting =  [dic[@"driver"][@"reting"]integerValue];
                                          order.token = dic[@"token"];
                                          order.date = dic[@"date_plane"];
                                          [orderList addObject:order];
                                      }
                                  }
                                  if (success) {
                                      success(orderList,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
    
}

- (void) getAllPoints:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray *points, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block NSMutableArray *points = nil;
    [self initWithUrl:url];
    
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      points = [[NSMutableArray alloc]init];
                                      points = [data valueForKey:@"point_list"];
                                  }
                                  
                                  if (success) {
                                      success(points,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) getFullInfo:(NSDictionary*)param
           onSuccess:(void(^)(RPOrderFull *orderFull, NSString *errorMessage)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block RPOrderFull *order = nil;
    [self initWithUrl:url];    
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
                                  NSDictionary *data = [response valueForKey:@"order_info"];
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      order = [[RPOrderFull alloc]init];
                                      order.numberOrder = [[data valueForKey:@"number_order"]integerValue];
                                      order.dateCreate = [data valueForKey:@"date_create"];
                                      order.dateFinish = [data valueForKey:@"date_finish"];
                                      order.summ = [[data valueForKey:@"summ"]integerValue];
                                      order.summBall = [[data valueForKey:@"summ_ball"]integerValue];
                                      order.addBall = [[data valueForKey:@"add_ball"]integerValue];
                                      order.itog = [[data valueForKey:@"itog"]integerValue];
                                      order.marka = [data valueForKey:@"marka"];
                                      order.numberCar = [data valueForKey:@"number_car"];
                                      order.ocenka = [[data valueForKey:@"ocenka"]integerValue];
                                      order.comment = [data valueForKey:@"comment"];
                                      if ([response valueForKey:@"driver"][@"name"]!=nil) {
                                          order.driver = [[RPDriver alloc]init];
                                          order.driver.foto = [response valueForKey:@"driver"][@"foto"];
                                          order.driver.name = [response valueForKey:@"driver"][@"name"];
                                          order.driver.reting = [[response valueForKey:@"driver"][@"reting"]integerValue];
                                          if ([response valueForKey:@"driver"][@"phone"]!=nil) {
                                              order.driver.phone = [response valueForKey:@"driver"][@"phone"];
                                          }
                                      }
                                      order.wayList = [[NSMutableArray alloc]init];
                                      NSArray *wayList = [response valueForKey:@"way_list"];
                                      for (NSDictionary *dic  in wayList) {
                                          RPLocalAddress *addressFrom = [[RPLocalAddress alloc]init];
                                          if (dic[@"city_from"]!=nil) {
                                              addressFrom.city = dic[@"city_from"];
                                          }
                                          if (dic[@"street_from"]!=nil) {
                                              addressFrom.street = dic[@"street_from"];
                                          }
                                          if (dic[@"house_from"]!=nil) {
                                              addressFrom.house = dic[@"house_from"];
                                          }
                                          if (dic[@"korpus_from"]!=nil) {
                                              addressFrom.korpus = dic[@"korpus_from"];
                                          }
                                          if (dic[@"stroenie_from"]!=nil) {
                                              addressFrom.stroenie = dic[@"stroenie_from"];
                                          }
                                          
                                          RPLocalAddress *addressTo = [[RPLocalAddress alloc]init];
                                          if (dic[@"city_to"]!=nil) {
                                              addressTo.city = dic[@"city_to"];
                                          }
                                          if (dic[@"street_to"]!=nil) {
                                              addressTo.street = dic[@"street_to"];
                                          }
                                          if (dic[@"house_to"]!=nil) {
                                              addressTo.house = dic[@"house_to"];
                                          }
                                          if (dic[@"korpus_to"]!=nil) {
                                              addressTo.korpus = dic[@"korpus_to"];
                                          }
                                          if (dic[@"stroenie_to"]!=nil) {
                                              addressTo.stroenie = dic[@"stroenie_to"];
                                          }
                                          
                                          [order.wayList addObject:@[addressFrom,addressTo]];
                                          
                                      }
                                  }
                                  
                                  if (success) {
                                      success(order,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

-(void)setSigletonOrderWithData:(NSDictionary*)data{
    [RPOrder sharedOrder].numberOrder = [[data valueForKey:@"number_order"]integerValue];
    [RPOrder sharedOrder].status = [[data valueForKey:@"status"]integerValue];
    [RPOrder sharedOrder].statusText = [data valueForKey:@"status_text"];
    [RPOrder sharedOrder].token = [data valueForKey:@"token"];
    if ([data valueForKey:@"driver"][@"name"]!=nil) {
        [RPOrder sharedOrder].driver = [[RPDriver alloc]init];
        [RPOrder sharedOrder].driver.foto = data[@"driver"][@"foto"];
        [RPOrder sharedOrder].driver.lan = [data[@"driver"][@"lan"]floatValue];
        [RPOrder sharedOrder].driver.lon = [data[@"driver"][@"lng"]floatValue];
        [RPOrder sharedOrder].driver.name = data[@"driver"][@"name"];
        if ( data[@"driver"][@"phone"]!=nil) {
            [RPOrder sharedOrder].driver.phone =  data[@"driver"][@"phone"];
        }
        [RPOrder sharedOrder].driver.reting = [data[@"driver"][@"reting"]integerValue];
    }
    
    if ([data valueForKey:@"way_from"]!=nil) {
        [RPOrder sharedOrder].wayFrom = [[RPLocalAddress alloc]init];
        [RPOrder sharedOrder].wayFrom.city = data[@"way_from"][@"city"];
        [RPOrder sharedOrder].wayFrom.house = data[@"way_from"][@"house"];
        [RPOrder sharedOrder].wayFrom.korpus = data[@"way_from"][@"korpus"];
        [RPOrder sharedOrder].wayFrom.street = data[@"way_from"][@"street"];
        [RPOrder sharedOrder].wayFrom.stroenie = data[@"way_from"][@"stroenie"];
        [RPOrder sharedOrder].wayFrom.location = CLLocationCoordinate2DMake([data[@"way_from"][@"lan"]floatValue], [data[@"way_from"][@"lng"]floatValue]) ;
    }
    if ([data valueForKey:@"way_to"]!=nil) {
        [RPOrder sharedOrder].wayTo = [[RPLocalAddress alloc]init];
        [RPOrder sharedOrder].wayTo.city = data[@"way_to"][@"city"];
        [RPOrder sharedOrder].wayTo.house = data[@"way_to"][@"house"];
        [RPOrder sharedOrder].wayTo.korpus = data[@"way_to"][@"korpus"];
        [RPOrder sharedOrder].wayTo.street = data[@"way_to"][@"street"];
        [RPOrder sharedOrder].wayTo.stroenie = data[@"way_to"][@"stroenie"];
        [RPOrder sharedOrder].wayTo.location = CLLocationCoordinate2DMake([data[@"way_to"][@"lan"]floatValue], [data[@"way_to"][@"lng"]floatValue]) ;
    }
    if ([data valueForKey:@"date"]!=nil) {
        [RPOrder sharedOrder].date = data[@"date"];
    }
    if ([data valueForKey:@"time"]!=nil) {
        [RPOrder sharedOrder].time = data[@"time"];
    }
    if ([data valueForKey:@"message_text1"]!=nil) {
        [RPOrder sharedOrder].messageText1 = data[@"message_text1"];
    }
    if ([data valueForKey:@"message_text2"]!=nil) {
        [RPOrder sharedOrder].messageText2 = data[@"message_text2"];
    }
    
}


- (void) changePassword:(NSDictionary*)param
              onSuccess:(void(^)(NSString *errorMessage)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else{
                                      [RPUser sharedUser].password = data[@"user_info"][@"password"];
                                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                      [userDefaults setValue:[RPUser sharedUser].password forKey:@"password"];
                                      [userDefaults synchronize];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) rememberPassword:(NSDictionary*)param
              onSuccess:(void(^)(NSString *errorMessage)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) sendToken:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

@end
