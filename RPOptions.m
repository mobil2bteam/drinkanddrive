//
//  RPOptions.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOptions.h"

@implementation RPOptions
+ (RPOptions*) sharedOptions {
    static RPOptions* options = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        options = [[RPOptions alloc] init];
    });
    return options;
}

@end
