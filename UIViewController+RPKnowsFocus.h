//
//  UIViewController+RPKnowsFocus.h
//  DrinkAndDrive
//
//  Created by Ruslan on 1/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UIViewController (RPKnowsFocus)
-(void) willGetFocus;
-(BOOL)isReachility;
-(void)showErrorMessage:(NSString*)errorMessage;
-(void)showWarningMessage:(NSString*)errorMessage;
-(void)showSuccessMessage:(NSString*)message;
-(void)addLogoToNavigationBar;
-(void)addLeftBarButton;
@end
