//
//  RPServerManager.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPUser.h"
#import "RPOptions.h"
#import "RPCar.h"
#import "RPAddress.h"
#import "RPOrder.h"
#import "RPOrderFull.h"
@interface RPServerManager : NSObject
+ (RPServerManager*) sharedManager;
- (void) getRootCategoriesWithParameters:(NSDictionary*)param
                               onSuccess:(void(^)(RPUser *user, NSString *errorMessage)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getConfigsOnSuccess:(void(^)(RPOptions *options)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) getCarsWithParameters:(NSDictionary*)param
                     onSuccess:(void(^)(NSMutableArray <RPCar*> *cars, NSString *errorMessage)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
    
- (void) getCarWithParameters:(NSDictionary*)param
                onSuccess:(void(^)(RPCar *car, NSString *errorMessage)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure ;

- (void) removeCarWithParameters:(NSDictionary*)param
                       onSuccess:(void(^)(NSString *errorMessage)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) getAddressesWithParameters:(NSDictionary*)param
                          onSuccess:(void(^)(NSMutableArray <RPAddress*> *addresses, NSString *errorMessage)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getAddressWithParameters:(NSDictionary*)param
                        onSuccess:(void(^)(RPAddress *address, NSString *errorMessage)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) newOrder:(NSDictionary*)param
        onSuccess:(void(^)(RPOrder *order, NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getOrder:(NSDictionary*)param
        onSuccess:(void(^)(RPOrder *order, NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) sendMessageForDriver:(NSDictionary*)param
                    onSuccess:(void(^)(NSString *errorMessage)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) inviteFriend:(NSDictionary*)param
            onSuccess:(void(^)(NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) cancelOrder:(NSDictionary*)param
           onSuccess:(void(^)(NSString *errorMessage)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) closeOrderWithOcenka:(NSDictionary*)param
                    onSuccess:(void(^)(NSString *errorMessage)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getOrderList:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getAllPoints:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray *points, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getFullInfo:(NSDictionary*)param
           onSuccess:(void(^)(RPOrderFull *orderFull, NSString *errorMessage)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) checkOrder:(NSDictionary*)param
          onSuccess:(void(^)(NSString *token, NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) changePassword:(NSDictionary*)param
              onSuccess:(void(^)(NSString *errorMessage)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) rememberPassword:(NSDictionary*)param
              onSuccess:(void(^)(NSString *errorMessage)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) sendToken:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
@end
