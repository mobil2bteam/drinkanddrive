//
//  RPDriver.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface RPDriver : NSObject
@property (nonatomic, strong) NSString *foto;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, assign) NSInteger reting;
@property (nonatomic, assign) CGFloat lan;
@property (nonatomic, assign) CGFloat lon;
@end
