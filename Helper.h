//
//  Helper.h
//  DrinkAndDrive
//
//  Created by Ruslan on 1/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#ifndef Header_h
#define Header_h
// categories
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+RPKnowsFocus.h"
#import "UITextView+RPTextView.h"
#import "NSString+Category.h"
#import "UIImageView+AFNetworking.h"

//protocols
#import "RPChangeAddressTo.h"
#import "RPUpdateLocation.h"


// libs
#import "LMGeocoder.h"
#import "SCLAlertView.h"
#import "ARSPopover.h"
#import "ECSlidingViewController.h"
#import "MBProgressHUD.h"
#import "JJMaterialTextfield.h"
#import <AudioToolbox/AudioServices.h>
#import <GoogleMaps/GoogleMaps.h>
#import <AFNetworking.h>

//server manager
#import "RPServerManager.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

#endif /* Header_h */
