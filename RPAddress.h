//
//  RPAddress.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/4/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPAddress : NSObject
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, assign) NSInteger korpus;
@property (nonatomic, assign) NSInteger stroenie;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *house;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSString *street;
@end
