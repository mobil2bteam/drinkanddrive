//
//  OrderTableViewCell+RPFillCell.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/31/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "OrderTableViewCell.h"
#import "RPOrder.h"
@interface OrderTableViewCell (RPFillCell)
-(OrderTableViewCell*)fillCellWithOrder:(RPOrder*)order;
@end
