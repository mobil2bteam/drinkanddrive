//
//  RPOrderViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/18/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPOrder.h"
@interface RPOrderViewController : UIViewController
@property (nonatomic, strong) RPOrder *order;
-(void)updateStatus;
-(void)updateUserLocationWith:(CLLocation*)location;
@end
