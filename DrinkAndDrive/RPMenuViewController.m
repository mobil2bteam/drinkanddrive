#import "RPMenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPMenuTableViewCell.h"
#import "RPOptions.h"
#import "RPAboutInWebViewController.h"
#import "MapViewController.h"
#import "UIViewController+RPKnowsFocus.h"
@interface RPMenuViewController ()
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) NSArray *menuItemsImages;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;
@end

@implementation RPMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor colorWithRed:0.15 green:0.16 blue:0.19 alpha:1.0];
    self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    UINavigationController* topViewController =
    ((UINavigationController*)self.slidingViewController.topViewController);
    [topViewController.visibleViewController willGetFocus];
}

#pragma mark - Properties

- (NSArray *)menuItems {
    if (_menuItems) return _menuItems;
    
    _menuItems = @[@"Вызвать водителя",
                   @"Личный кабинет",
                   @"Тарифный план",
                   @"Получить бонусы",
                   @"Пользовательское соглашение",
                   @"О сервисе"];
    return _menuItems;
}

- (NSArray *)menuItemsImages {
    if (_menuItemsImages) return _menuItemsImages;
    
    _menuItemsImages = @[@"RoomGreen",
                   @"IdentityGreen",
                   @"Steering",
                   @"CardGreen",
                   @"Circle",
                   @"Schedule"];
    return _menuItemsImages;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MenuCell";
    RPMenuTableViewCell *cell = (RPMenuTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row==0) {
        cell.logoImageView.image=nil;
        cell.logoImageView.image = [UIImage imageNamed:@"LogoDark"];
        [cell setBackgroundColor:[UIColor colorWithRed:0.49 green:1.00 blue:0.24 alpha:1.0]];

    } else {
        cell.menuItemImageView.image=nil;
        cell.menuItemImageView.image = [UIImage imageNamed:self.menuItemsImages[indexPath.row-1]];
        cell.menuLabel.text = self.menuItems[indexPath.row-1];
        [cell setBackgroundColor:[UIColor colorWithRed:0.15 green:0.16 blue:0.19 alpha:1.0]];
    }
    if (indexPath.row!=1 && indexPath.row!=4) {
        cell.separatorView.backgroundColor = [UIColor clearColor];
    } else {
        cell.separatorView.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:0.3];

    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 140.f;
    } else return 60.f;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 1:
        {
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"token2"]==nil) {
                self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"METransitionsNavigationController"];
            } else {
                UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"METransitionsNavigationController"];
                MapViewController *mapContoller = navController.viewControllers[0];
                [mapContoller pushOrderViewController:nil];
                self.slidingViewController.topViewController = navController;
            }
        }
            break;
        case 2:
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"hh"];
            break;
        case 3:
        {
            UINavigationController *navControler = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutInWebNavController"];
            RPAboutInWebViewController *aboutController = navControler.viewControllers[0];
            aboutController.urlAddress =  [RPOptions sharedOptions].user_rates;
            self.slidingViewController.topViewController = navControler;
        }
            break;
        case 4:
        {
            UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"hh"];
            navController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"inviteViewController"]];
            self.slidingViewController.topViewController = navController;
        }
            break;
        case 5:
        {
            UINavigationController *navControler = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutInWebNavController"];
            RPAboutInWebViewController *aboutController = navControler.viewControllers[0];
            aboutController.urlAddress =  [RPOptions sharedOptions].user_agreement;
            self.slidingViewController.topViewController = navControler;
        }
            
            break;
        case 6:
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutNavController"];
            break;

        default:
            break;
    }
        [self.slidingViewController resetTopViewAnimated:YES];
}

@end
