//
//  RPEditCarViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/3/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RPReloadDelegate;

@interface RPEditCarViewController : UIViewController
@property (nonatomic, assign) BOOL isEditCarMode;
@property (nonatomic, strong) NSNumber *carId;
@property (nonatomic, weak) id<RPReloadDelegate> delegate;
@end

@protocol RPReloadDelegate <NSObject>
@required
- (void)reload;
@end
