//
//  OrdersViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrderTableViewCell.h"
#import "Helper.h"
#import "RPOrderTabBarController.h"
#import "OrderTableViewCell+RPFillCell.h"

@interface OrdersViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <RPOrder*> *orderList;
@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *param = @{@"type":@"orderlist",
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getOrderList:param onSuccess:^(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (errorMessage==nil) {
                self.orderList = orderList;
                [self.tableView reloadData];
            } else [self showErrorMessage:@"Произошла ошибка"];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [self showErrorMessage:@"Произошла ошибка"];
        }];

    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    }
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.orderList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderTableViewCell *cell = (OrderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    RPOrder *order = self.orderList[indexPath.row];
    return [cell fillCellWithOrder:order];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 61;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"showDescriptionOrder"]) {
        RPOrderTabBarController *tabBarController = segue.destinationViewController;
        RPOrder *order = self.orderList[[self.tableView indexPathForSelectedRow].row];
        tabBarController.token = order.token;
    }
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"showDescriptionOrder"]) return YES;
    return NO;
}
@end
