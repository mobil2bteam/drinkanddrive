//
//  RPOrderAddressesTableViewCell.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderAddressesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressALabel;
@property (weak, nonatomic) IBOutlet UILabel *addressBLabel;
@end
