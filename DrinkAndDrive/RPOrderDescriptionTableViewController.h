//
//  RPOrderDescriptionTableViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPServerManager.h"
#import "RPUser.h"
#import "RPOrderTabBarController.h"
#import "UIImageView+AFNetworking.h"

@interface RPOrderDescriptionTableViewController : UITableViewController
@property (nonatomic, strong) RPOrderFull *orderFull;
@end
