//
//  RPEditCarViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/3/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPEditAddressViewController.h"
#import "Helper.h"
@interface RPEditAddressViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *streetTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *housingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *houseTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *buildingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *commentTextField;
@property (nonatomic, strong) RPAddress *currentAddress;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *cityTextField;

@end

@implementation RPEditAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.isEditAddressMode) {
        [self.deleteButton removeFromSuperview];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.isEditAddressMode) {
        [self loadCars];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.cityTextField) {
        [self.streetTextField becomeFirstResponder];
    }
    if (textField == self.streetTextField) {
        [self.houseTextField becomeFirstResponder];
    }
    if (textField == self.houseTextField) {
        [self.housingTextField becomeFirstResponder];
    }
    if (textField == self.housingTextField) {
        [self.buildingTextField becomeFirstResponder];
    }
    if (textField == self.buildingTextField) {
        [self.commentTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField == self.streetTextField || textField == self.houseTextField) {
        if (textField.text.length==0) {
            [textField showError];
        }else{
            [textField hideError];
        }
    }
}

- (IBAction)deleteButtonPressed:(id)sender {
    NSDictionary *params = @{@"type":@"removeaddress",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password,
                             @"addressid":_addressId};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] removeCarWithParameters:params onSuccess:^(NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else
            {
                [self.delegate reload];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];

    });

    
    
}
- (IBAction)saveButtonPressed:(id)sender {
    if (self.streetTextField.text.length==0 || self.cityTextField.text.length==0 || self.houseTextField.text.length==0) {
        if (self.streetTextField.text.length==0) {
            [self.streetTextField showError];
        }
        if (self.cityTextField.text.length==0) {
            [self.cityTextField showError];
        }
        if (self.houseTextField.text.length==0) {
            [self.houseTextField showError];
        }
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    params[@"userid"]= @([RPUser sharedUser].userid);
    params[@"password"]= [RPUser sharedUser].password;
    params[@"city"]= self.cityTextField.text;
    params[@"street"]= self.streetTextField.text;
    params[@"house"]= self.houseTextField.text;
    if (self.housingTextField.text.length!=0) {
        params[@"korpus"]= self.housingTextField.text;
    }
    if (self.buildingTextField.text.length!=0) {
        params[@"stroenie"]= self.buildingTextField.text;
    }
    if (self.commentTextField.text.length!=0) {
        params[@"comment"]= self.commentTextField.text;
    }
    if (self.isEditAddressMode) {
        params[@"type"]= @"editaddress";
        params[@"addressid"]= _addressId;

    } else params[@"type"]= @"addaddress";    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getAddressWithParameters:params onSuccess:^(RPAddress *address, NSString *errorMessage) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                [self.delegate reload];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}

-(void)loadCars{
    
    
    NSDictionary *params = @{@"type":@"getaddress",
                             @"userid":@([RPUser sharedUser].userid),
                             @"addressid":_addressId,
                             @"password":[RPUser sharedUser].password};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getAddressWithParameters:params onSuccess:^(RPAddress *address, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                self.currentAddress = address;
                self.cityTextField.text=address.city;
                self.streetTextField.text= address.street;
                self.houseTextField.text = address.house;
                self.commentTextField.text = address.comment;
                if (address.korpus!=0) {
                    self.housingTextField.text = [NSString stringWithFormat:@"%ld", (long)address.korpus];
                }
                if (address.stroenie!=0) {
                    self.buildingTextField.text = [NSString stringWithFormat:@"%ld", (long)address.stroenie];
                }
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });

}

-(BOOL)validateTextFields{
    if (self.streetTextField.text.length==0 | self.housingTextField.text==0)
        return NO;
    return YES;
}
@end
