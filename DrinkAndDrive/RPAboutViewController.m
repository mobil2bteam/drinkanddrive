//
//  RPAboutViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPAboutViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPOptions.h"
#import "UIViewController+RPKnowsFocus.h"
@interface RPAboutViewController()
@property (weak, nonatomic) IBOutlet UIButton *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *siteLabel;

@end
@implementation RPAboutViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addLogoToNavigationBar];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[self.phoneLabel attributedTitleForState:UIControlStateNormal]];
    [attributedString replaceCharactersInRange:NSMakeRange(0, attributedString.length) withString:[RPOptions sharedOptions].about_phone];
    [self.phoneLabel setAttributedTitle:attributedString forState:UIControlStateNormal];
    
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc] initWithAttributedString:[self.siteLabel attributedTitleForState:UIControlStateNormal]];
    [attributedString2 replaceCharactersInRange:NSMakeRange(0, attributedString2.length) withString:[RPOptions sharedOptions].about_suite];
    [self.siteLabel setAttributedTitle:attributedString2 forState:UIControlStateNormal];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}


- (IBAction)callNumberButtonPressed:(id)sender {
    NSArray* words = [[RPOptions sharedOptions].about_phone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)openLinkButtonPressed:(id)sender {
    NSString *url = [NSString stringWithFormat:@"http://www.drinkndrive.ru"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

}
- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}
@end
