//
//  RPChangePasswordViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/6/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPChangePasswordViewController.h"
#import "Helper.h"

@interface RPChangePasswordViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *passwordTextField;
@end

@implementation RPChangePasswordViewController

- (IBAction)changePasswordPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.passwordTextField.text.length!=0 && self.oldPasswordTextField.text.length!=0) {
        NSDictionary *dic = @{@"type":@"changepassword",
                              @"id":@([RPUser sharedUser].userid),
                              @"oldpassword":[self.oldPasswordTextField.text md5],
                              @"newpassword":[self.passwordTextField.text md5]
                                      };
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Загрузка";
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [[RPServerManager sharedManager]changePassword:dic onSuccess:^(NSString *errorMessage) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (errorMessage==nil) {
                    [self showSuccessMessage:@"Ваш пароль успешно изменен!"];
                } else [self showErrorMessage:errorMessage];
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self showErrorMessage:@"Произошла ошибка"];
                
            }];
        });
    } else {
            if (self.oldPasswordTextField.text.length==0) {
                [self.oldPasswordTextField showError];
            }
            if (self.passwordTextField.text.length!=10) {
                [self.passwordTextField showError];
            }
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.oldPasswordTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
}

-(BOOL)validateTextFieldsFromLogin:(BOOL)login{
    if (self.passwordTextField.text.length==0 || self.oldPasswordTextField.text==0)
        return NO;
    return YES;
}
@end
