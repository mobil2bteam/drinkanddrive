//
//  OrderTableViewCell+RPFillCell.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/31/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "OrderTableViewCell+RPFillCell.h"
#import "UIImageView+AFNetworking.h"

@implementation OrderTableViewCell (RPFillCell)
-(OrderTableViewCell*)fillCellWithOrder:(RPOrder*)order{
    self.orderImageView.image = nil;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.pobegun.ru/%@",order.driver.foto]];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.orderImageView setImageWithURLRequest:imageRequest
                               placeholderImage:nil                                          success:nil
                                        failure:nil];
    
    self.fromLabel.text = order.adddressA;
    self.toLabel.text = order.adddressB;
    self.dateLabel.text =order.date;
    return self;
}
@end
