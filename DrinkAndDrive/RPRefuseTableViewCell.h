//
//  RPRefuseTableViewCell.h
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRefuseTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *refuseLabel;

@end
