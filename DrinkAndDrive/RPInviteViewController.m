//
//  RPInviteTableViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPInviteViewController.h"
#import "Helper.h"

@interface RPInviteViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *balLabel;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField4;
@property (nonatomic, strong) NSString *phone;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *emailTextField;
@end

@implementation RPInviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray *leftButtons = [NSMutableArray arrayWithArray: self.navigationItem.leftBarButtonItems];
    if (self.needHiddenMenuButton) {
        UIBarButtonItem *item = leftButtons[0];
        if (item.tag == 1) {
            [leftButtons removeObjectAtIndex:0];
        }
        self.navigationItem.leftBarButtonItems = leftButtons;
    }
    [self addLogoToNavigationBar];
    self.balLabel.text = [NSString stringWithFormat:@"%ld", (long)[RPOptions sharedOptions].count_ball];
    self.tableView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.needHiddenMenuButton) {
            [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
        }
}

- (IBAction)inviteButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.emailTextField.text.length==0 || self.nameTextField.text.length==0 || self.phone.length != 10) {
        if (self.nameTextField.text.length==0) {
            [self.nameTextField showError];
        }
        if (self.emailTextField.text.length==0) {
            [self.emailTextField showError];
        }
        if (self.phone.length != 10) {
            [self showErrorMessage:@"Неправильный телефон"];
        }
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }

    NSDictionary *param = @{@"type":@"addfriend",
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            @"name":self.nameTextField.text,
                            @"email":self.emailTextField.text,
                            @"phone":self.phone,
                            };
        
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager]inviteFriend:param onSuccess:^(NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage==nil) {
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
                alert.backgroundType = Blur;
                alert.shouldDismissOnTapOutside = NO;
                [alert addButton:@"Ok" actionBlock:^{
                    self.nameTextField.text=self.phoneTextField1.text=self.phoneTextField2.text=self.phoneTextField3.text=self.phoneTextField4.text=self.emailTextField.text=@"";
                    [self.nameTextField hideError];
                    [self.emailTextField hideError];
                    [self.nameTextField becomeFirstResponder];
                }];
                [alert showInfo:self title:nil subTitle:@"Спасибо! Мы пригласим вашего друга!" closeButtonTitle:nil duration:0.0f];
                            } else {
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}
- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}


#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.nameTextField) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.emailTextField becomeFirstResponder];
    }

    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        return;
    }
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
}

-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            if (textField == self.phoneTextField4) {
                [self.emailTextField becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}
@end
