//
//  RPQuickOrderViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 1/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface RPQuickOrderViewController : UIViewController<RPChangeAddressTo,RPUpdateLocation>
-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate;
@end
