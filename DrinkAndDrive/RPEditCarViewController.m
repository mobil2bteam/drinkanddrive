//
//  RPEditCarViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/3/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPEditCarViewController.h"
#import "Helper.h"


@interface RPEditCarViewController ()<UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate, ARSPopoverDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *carModelTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *carNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (nonatomic, strong) RPCar *currentCar;
@property (weak, nonatomic) IBOutlet UISwitch *carSwitch;
@property (weak, nonatomic) IBOutlet UIButton *typeCarButton;
@property (nonatomic, assign) NSInteger selectedCarTypeIndex;
@property (strong) ARSPopover *popover;
@end

@implementation RPEditCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.isEditCarMode) {
        [self.deleteButton removeFromSuperview];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.isEditCarMode) {
        [self loadCars];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.carModelTextField) {
        [self.carNumberTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
}


- (IBAction)deleteButtonPressed:(id)sender {
    NSDictionary *params = @{@"type":@"removecar",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password,
                             @"carid":_carId};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] removeCarWithParameters:params onSuccess:^(NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                [self.delegate reload];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}
- (IBAction)saveButtonPressed:(id)sender {
    if (self.carNumberTextField.text.length==0 || self.carModelTextField.text.length==0) {
        if (self.carNumberTextField.text.length==0) {
            [self.carNumberTextField showError];
        }
        if (self.carModelTextField.text.length==0) {
            [self.carModelTextField showError];
        }
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    
    NSInteger typeCar = [[RPOptions sharedOptions].typecar[self.selectedCarTypeIndex][@"id"]integerValue];
    
    NSDictionary *params;
    
    if (self.isEditCarMode) {
        params = @{@"type":@"editcar",
                   @"userid":@([RPUser sharedUser].userid),
                   @"password":[RPUser sharedUser].password,
                   @"carid":_carId,
                   @"marka":self.carModelTextField.text,
                   @"number":self.carNumberTextField.text,
                   @"typecar":@(typeCar),
                   @"default":@(self.carSwitch.on)};
        
    } else params = @{@"type":@"addcar",
                      @"userid":@([RPUser sharedUser].userid),
                      @"password":[RPUser sharedUser].password,
                      @"marka":self.carModelTextField.text,
                      @"number":self.carNumberTextField.text,
                      @"typecar":@(typeCar),
                      @"default":@(self.carSwitch.on)};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getCarWithParameters:params onSuccess:^(RPCar *car, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                [self.delegate reload];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}

-(void)loadCars{
    NSDictionary *params = @{@"type":@"getcar",
                             @"userid":@([RPUser sharedUser].userid),
                             @"carid":self.carId,
                             @"password":[RPUser sharedUser].password};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getCarWithParameters:params onSuccess:^(RPCar *car, NSString *errorMessage) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                self.currentCar = car;
                self.carNumberTextField.text= car.numberCar;
                self.carModelTextField.text = car.marka;
                self.selectedCarTypeIndex = 0;
                NSString *type = @"";
                for (NSInteger index = 0; index < [RPOptions sharedOptions].typecar.count; index ++) {
                    NSDictionary *dictionary = [RPOptions sharedOptions].typecar[index];
                    if ([dictionary[@"id"]integerValue] == car.typeCar) {
                        type = dictionary[@"name"];
                        self.selectedCarTypeIndex = index;
                    }
                }
                [self.typeCarButton setTitle:type forState:UIControlStateNormal];
                [self.carSwitch setOn:car.def==1];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка!"];
        }];
    });
    
    
    
}

#pragma mark - UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [RPOptions sharedOptions].typecar.count;
}


- (NSAttributedString *)pickerView:(UIPickerView *)pickerView
             attributedTitleForRow:(NSInteger)row
                      forComponent:(NSInteger)component
{
    NSString *string = [[RPOptions sharedOptions].typecar[row] valueForKey:@"name"];
    
    
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@
            {
            NSForegroundColorAttributeName:[UIColor whiteColor]
            }];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[RPOptions sharedOptions].typecar[row] valueForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedCarTypeIndex = row;
}

#pragma mark  - <ARSPopoverDelegate>
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    [self.typeCarButton setTitle:[[RPOptions sharedOptions].typecar[self.selectedCarTypeIndex] valueForKey:@"name"] forState:UIControlStateNormal];
}
- (IBAction)typeCarButtonPressed:(UIButton*)button {
    self.popover = [ARSPopover new];
    self.popover.sourceView = self.typeCarButton;
    self.popover.sourceRect = CGRectMake(CGRectGetMaxX(self.typeCarButton.bounds), CGRectGetMaxY(self.typeCarButton.bounds), 0, 0);
    self.popover.contentSize = CGSizeMake(150, 100);
    self.popover.arrowDirection = UIPopoverArrowDirectionUp;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            pickerView.backgroundColor = [UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
            pickerView.tintColor= [UIColor whiteColor];
            [pickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:self.selectedCarTypeIndex inComponent:0 animated:YES];
            [popover.view addSubview:pickerView];
        }
         ];
    }];
    
}


@end
