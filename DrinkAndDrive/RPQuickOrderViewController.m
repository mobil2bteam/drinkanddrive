//
//  RPQuickOrderViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 1/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPQuickOrderViewController.h"
#import "RPSearchAddressViewController.h"
#import "RPSearchFullAddressViewController.h"
#define klocation  self.appDelegate.locationManager.location.coordinate
@interface RPQuickOrderViewController()<UITextFieldDelegate>
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSUserDefaults *userDefaults;
@property (nonatomic, strong) LMAddress *addressTo;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField4;
@property (nonatomic, strong) NSString *phone;
@end

@implementation RPQuickOrderViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addLogoToNavigationBar];
    [self addLeftBarButton];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.appDelegate.delegate=self;
    self.addressTo=nil;
    self.addressLabel.text = @"Определяем...";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    if ([self.userDefaults stringForKey:@"phone"] && !self.phone.length) {
        NSString *phone = [self.userDefaults stringForKey:@"phone"];
        self.phoneTextField1.text = [phone substringWithRange:NSMakeRange(0, 3)];
        self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
        self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
        self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
    }

}

#pragma mark - Actions
- (IBAction)quickOrderButtonPressed:(id)sender {
    if ([self.addressTo isCorrectAddress]==NO){
        RPSearchAddressViewController *newController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchAddressViewController"];
        newController.addressDelegate = self;
        newController.isQuickOrder=YES;
        newController.address=self.addressTo;
        [self.navigationController pushViewController:newController animated:YES];
        return;
    }
    if (self.phone.length != 10) {
        [self showErrorMessage:@"Неправильный телефон"];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    NSMutableDictionary *param = [@{@"type":@"new",
                                    @"user_phone":self.phone,
                                    @"flag_reg":@(0),
                                    @"flag_time":@(1),
                                    @"lan_user":@(self.appDelegate.locationManager.location.coordinate.latitude),
                                    @"lng_user":@(self.appDelegate.locationManager.location.coordinate.longitude),
                                    @"city_from":self.addressTo.locality,
                                    @"street_from":self.addressTo.street,
                                    @"house_from":self.addressTo.house,
                                    @"lan_from":@(self.addressTo.coordinate.latitude),
                                    @"lng_from":@(self.addressTo.coordinate.longitude)
                                    }mutableCopy];
    if (self.addressTo.korpus.length!=0) {
        param[@"korpus_from"]=self.addressTo.korpus;
    }
    if (![NSString isEmpty:self.appDelegate.registrationToken]) {
        param[@"gcm_regid"]=self.appDelegate.registrationToken;
    }
    if (self.addressTo.stroenie.length!=0) {
        param[@"stroenie_from"]=self.addressTo.stroenie;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager]newOrder:param onSuccess:^(RPOrder *order, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage==nil) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:[RPOrder sharedOrder].token forKey:@"token"];
                [userDefaults synchronize];
                [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"rporderviewcontroller"] animated:YES completion:nil];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}

- (IBAction)changeAddressButtonPressed:(id)sender {
    RPSearchAddressViewController *newController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchAddressViewController"];
    newController.addressDelegate = self;
    newController.isQuickOrder=YES;
    newController.address=self.addressTo;
    [self.navigationController pushViewController:newController animated:YES];
}
- (IBAction)callOperator:(id)sender {
    NSArray* words = [[RPOptions sharedOptions].operatorPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}
#pragma mark - Methods

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate{
    if (self.addressTo==nil) {
        [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:coordinate                                                  service:kLMGeocoderGoogleService
                                            completionHandler:^(NSArray *results, NSError *error) {
                                                if (results.count && !error) {
                                                    LMAddress *address = [results firstObject];
                                                    if (![NSString isEmpty:[address parseThoroughfare]]) {
                                                        self.addressTo = address;
                                                        self.addressLabel.text = [NSString stringWithFormat:@"%@ %@", address.locality,                                                        [address parseThoroughfare]];
                                                    }
                                                }
                                            }];
    }
    
}

#pragma mark - RPChangeAddressDelegate
-(void)passAddressTo:(LMAddress *)address{
    self.addressTo = address;
    if (![NSString isEmpty:[address parseThoroughfare]]) {
        [self.addressLabel setText:[NSString stringWithFormat:@"%@ %@",address.locality, [address parseThoroughfare]
                                    ]];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}


@end
