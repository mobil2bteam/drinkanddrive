//
//  RPEditAddressViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/4/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPEditCarViewController.h"
@interface RPEditAddressViewController : UIViewController
@property (nonatomic, assign) BOOL isEditAddressMode;
@property (nonatomic, strong) NSNumber *addressId;
@property (nonatomic, weak) id<RPReloadDelegate> delegate;
@end
