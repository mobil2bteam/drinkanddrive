//
//  RPOrderRouteViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrderRouteViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RPServerManager.h"
#import "RPUser.h"
#import "RPOrderTabBarController.h"

@interface RPOrderRouteViewController()
@property (nonatomic, strong) NSMutableArray *points;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@end
@implementation RPOrderRouteViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSDictionary *param = @{@"type":@"get_all_point",
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            @"token":((RPOrderTabBarController*)self.tabBarController).token
                            };
    [[RPServerManager sharedManager] getAllPoints:param onSuccess:^(NSMutableArray *points, NSString *errorMessage) {
        self.points = points;
        CLLocationCoordinate2D mapCenter = CLLocationCoordinate2DMake( ([[[points firstObject]valueForKey:@"lan"]floatValue] +  [[[points lastObject]valueForKey:@"lan"]floatValue])/2.0,  ([[[points firstObject]valueForKey:@"lng"]floatValue] +  [[[points lastObject]valueForKey:@"lng"]floatValue])/2.0);

        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:mapCenter.latitude                                                                longitude:mapCenter.longitude
                                                                     zoom:12];
        [self.mapView animateToCameraPosition:camera];

        GMSMarker *markerA = [[GMSMarker alloc] init];
        markerA.position = CLLocationCoordinate2DMake([[[points firstObject]valueForKey:@"lan"]floatValue],  [[[points firstObject]valueForKey:@"lng"]floatValue]);
        markerA.appearAnimation = kGMSMarkerAnimationPop;
        markerA.icon = [UIImage imageNamed:@"PointADark"];
        markerA.map = self.mapView;
        
        GMSMarker *markerB = [[GMSMarker alloc] init];
        markerB.position = CLLocationCoordinate2DMake([[[points lastObject]valueForKey:@"lan"]floatValue],[[[points lastObject]valueForKey:@"lng"]floatValue]);
        markerB.appearAnimation = kGMSMarkerAnimationPop;
        markerB.icon = [UIImage imageNamed:@"PointBDark"];
        markerB.map = self.mapView;

        
        GMSMutablePath *path = [GMSMutablePath path];
        for (NSDictionary *dic in points) {
            
            [path addCoordinate:CLLocationCoordinate2DMake([dic[@"lan"]floatValue], [dic[@"lng"]floatValue])];
        }
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeColor = [UIColor redColor];
        polyline.strokeWidth = 8.f;
        polyline.map = self.mapView;
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];

}
@end
