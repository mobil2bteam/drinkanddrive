//
//  MapViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/18/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPSearchFullAddressViewController.h"
#import "Helper.h"
@interface MapViewController : UIViewController<ECSlidingViewControllerDelegate,RPSearchFullAddressViewControllerDelegate,RPUpdateLocation>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
- (IBAction)menuButtonTapped:(id)sender;
-(void)pushOrderViewController:(RPOrder*)order;
@end


