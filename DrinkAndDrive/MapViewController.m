

#import "MapViewController.h"
#import "RPAdditionalDataViewController.h"
#import "RPOrderViewController.h"
#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height
#define kAnimationDuration 1.0f
#define klocation  self.appDelegate.locationManager.location.coordinate

@interface MapViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *callDriverButton;
@property (nonatomic, strong) GMSMarker *marker;
@property (nonatomic, strong) NSString *addressToParse;
@property (nonatomic, strong) LMAddress *addressFrom;
@property (nonatomic, strong) AppDelegate *appDelegate;
@end

@implementation MapViewController

#pragma mark - Lifecycle
-(void)viewDidLoad
{
    [super viewDidLoad];
    [self addLogoToNavigationBar];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.appDelegate.delegate=self;
    self.addressFrom = nil;
    [self prepareMapView];

}

-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate{
    if (self.addressFrom==nil) {
        [self.mapView animateToCameraPosition:[GMSCameraPosition
                                               cameraWithLatitude:coordinate.latitude
                                               longitude:coordinate.longitude
                                               zoom:17]];
        if (self.marker==nil) {
            self.marker = [[GMSMarker alloc] init];
            self.marker.position = coordinate;
            self.marker.appearAnimation = kGMSMarkerAnimationPop;
            self.marker.icon = [UIImage imageNamed:@"There"];
            [self.marker setDraggable: NO];
            self.marker.map=self.mapView;
            
            
            [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:coordinate                                                  service:kLMGeocoderGoogleService
                                                completionHandler:^(NSArray *results, NSError *error) {
                                                    if (results.count && !error) {
                                                        LMAddress *address = [results firstObject];
                                                        if (![NSString isEmpty:[address parseThoroughfare]]) {
                                                            self.addressFrom = address;
                                                            self.addressLabel.text = [NSString stringWithFormat:@"%@ %@", address.locality,                                                        [address parseThoroughfare]];
                                                        }
                                                    }
                                                }];

        }
}
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=NO;
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

-(void)prepareMapView{
    GMSCameraPosition *camera = [GMSCameraPosition
                                 cameraWithLatitude:55.755826
                                 longitude: 37.6173
                                 zoom:15];
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    self.mapView.settings.consumesGesturesInView=YES;
}


#pragma mark - RPSearchFullAddressViewControllerDelegate
- (void)passAddressFrom:(LMAddress *)addressFrom{
    self.addressFrom = addressFrom;
    self.marker.position = addressFrom.coordinate;
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",addressFrom.locality, [addressFrom parseThoroughfare]];
    [self animateToMarkerPosition];
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"searchFullAddressSegue"]) {
        RPSearchFullAddressViewController *newController = segue.destinationViewController;
        newController.addressFrom = self.addressFrom;
        newController.delegate = self;
        newController.latitude  = klocation.latitude;
        newController.longitude = klocation.longitude;
    }
    
    if ([segue.identifier isEqualToString:@"additionalDataSegue"]) {
        RPAdditionalDataViewController *newController = segue.destinationViewController;
        newController.addressFrom = self.addressFrom;
        newController.latitude  = klocation.latitude;
        newController.longitude = klocation.longitude;
    }

}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"searchFullAddressSegue"]) {
            return YES;
    }
    if ([identifier isEqualToString:@"additionalDataSegue"]) {
        return YES;
    }
    return NO;
}


#pragma mark - Actions

- (IBAction)getCurrentLocation:(id)sender {
    [self animateToMarkerPosition];
    }

-(void)animateToMarkerPosition{
    [self.mapView animateToCameraPosition:[GMSCameraPosition
                                           cameraWithLatitude:self.marker.position.latitude
                                           longitude:self.marker.position.longitude
                                           zoom:self.mapView.camera.zoom]];
}

- (IBAction)callDriverButtonPressed:(id)sender {
    if ([self.addressFrom isCorrectAddress])
        [self performSegueWithIdentifier:@"additionalDataSegue" sender:self];
    else [self performSegueWithIdentifier:@"searchFullAddressSegue" sender:self];
}


- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

-(void)pushOrderViewController:(RPOrder*)order{
    RPOrderViewController *newController = [self.storyboard instantiateViewControllerWithIdentifier:@"rporderviewcontroller"];
    newController.order = order;
    [self.navigationController pushViewController:newController animated:NO];
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:coordinate                                                  service:kLMGeocoderGoogleService
                                        completionHandler:^(NSArray *results, NSError *error) {
                                            if (results.count && !error) {
                                                LMAddress *address = [results firstObject];
                                                self.addressFrom = address;
                                                [self.addressFrom parseThoroughfare];
                                                if (![NSString isEmpty:[self.addressFrom parseThoroughfare]]) {
                                                     self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",self.addressFrom.locality, [self.addressFrom parseThoroughfare]];
                                                } else self.addressLabel.text = self.addressFrom.locality;
                                               
                                                }
                                        }];
    if (self.marker==nil) {
        self.marker = [[GMSMarker alloc] init];
        self.marker.position = coordinate;
        self.marker.appearAnimation = kGMSMarkerAnimationPop;
        self.marker.icon = [UIImage imageNamed:@"There"];
        [self.marker setDraggable: NO];
        self.marker.map=self.mapView;
    } else self.marker.position = coordinate;
    [self animateToMarkerPosition];
}

@end
