//
//  main.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
