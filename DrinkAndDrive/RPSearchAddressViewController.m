//
//  RPSearchAddressViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/8/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPSearchAddressViewController.h"
#import "RPTabBarViewController.h"
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height

@interface RPSearchAddressViewController ()<CLLocationManagerDelegate, UITextFieldDelegate,GMSMapViewDelegate>
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *cityTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *streetTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *homeTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *housingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *buildingTextField;
@property (weak, nonatomic) IBOutlet UIButton *showButton;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) GMSMarker *marker;
@property (nonatomic, strong) GMSPolyline *singleLine;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewTopMarginConstraint;
@property (strong , nonatomic) NSString *addressText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeightConstraint;
@end

@implementation RPSearchAddressViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.streetTextField.delegate = self;
    if (kScreenHeight>480) {
        if (self.isQuickOrder) {
            self.mapViewHeightConstraint.constant = kScreenHeight - 224 - 352 + 138 + 11 + 49;
        } else self.mapViewHeightConstraint.constant = kScreenHeight - 224 - 352 + 138 + 11;
        [self.view layoutIfNeeded];
    }
    self.mapView.delegate=self;
    [self prepareMapView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=NO;
    if (!self.isQuickOrder) self.address = ((RPTabBarViewController*)self.tabBarController).addressTo;
    if (self.address!=nil) {
        if (![NSString isEmpty:self.address.locality]) {
            self.cityTextField.text = self.address.locality;
        }
        if (![NSString isEmpty:self.address.street]) {
            self.streetTextField.text = self.address.street;
        }
        if (![NSString isEmpty:self.address.house]) {
            self.homeTextField.text = self.address.house;
        }
        if (![NSString isEmpty:self.address.korpus]) {
            self.housingTextField.text = self.address.korpus;
        }
        if (![NSString isEmpty:self.address.stroenie]) {
            self.buildingTextField.text = self.address.stroenie;
        }
        self.marker = [[GMSMarker alloc] init];
        self.marker.position = self.address.coordinate;
        if (!self.isQuickOrder) {
            self.marker.snippet = @"Мне сюда";
            [self createPath];
        } self.marker.snippet=@"Тут";
        self.marker.appearAnimation = kGMSMarkerAnimationPop;
        self.marker.map = self.mapView;
        self.marker.icon = [UIImage imageNamed:@"There"];
        [self animateToMarkerPosition];
    } else self.cityTextField.text = @"Москва";
}

- (IBAction)showButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.mapViewTopMarginConstraint.constant!=0) {
        self.mapViewTopMarginConstraint.constant = 0;
        
    } else{
        self.mapViewTopMarginConstraint.constant = 214;
    }
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished) {
        if (self.mapViewTopMarginConstraint.constant==0) {
            self.showButton.titleLabel.text=@"СКРЫТЬ КАРТУ";
            if (self.marker!=nil) {
                if (![NSString isEmpty:[self.address parseThoroughfare]]) {
                    [self.saveButton setTitle:[NSString stringWithFormat:@"%@ %@", self.address.locality, [self.address parseThoroughfare]] forState:UIControlStateNormal];
                    
                } else [self.saveButton setTitle:self.address.locality forState:UIControlStateNormal];
            }
        } else {
            self.showButton.titleLabel.text=@"РАСКРЫТЬ КАРТУ";
            [self.saveButton setTitle:@"СОХРАНИТЬ" forState:UIControlStateNormal];
        }
    }];
    
}

-(void)prepareMapView{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.mapView.myLocationEnabled=NO;
    self.mapView.delegate=self;
    CLLocationCoordinate2D coordinate =  self.locationManager.location.coordinate;
    GMSCameraPosition *camera = [GMSCameraPosition
                                 cameraWithLatitude:coordinate.latitude                                 longitude: coordinate.longitude
                                 zoom:17];
    self.mapView.camera = camera;
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.cityTextField) {
        if (!self.streetTextField.text.length) {
            [self.streetTextField becomeFirstResponder];
        }
        if (self.streetTextField.text.length && !self.homeTextField.text.length) {
            [self.homeTextField becomeFirstResponder];
        }
    } else if (textField == self.streetTextField) {
        if (!self.homeTextField.text.length) {
            [self.homeTextField becomeFirstResponder];
        }
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
    
    if (self.cityTextField.text.length!=0 && self.streetTextField.text.length!=0 && self.homeTextField.text.length!=0) {
        [self getLocationFromAddress];
    }
}

-(void)getLocationFromAddress{
    self.address = nil;
    
    NSMutableString *sentence = [[NSString stringWithFormat:@"Россия,%@,%@,%@",_cityTextField.text,_streetTextField.text,_homeTextField.text]mutableCopy];
    if (self.housingTextField.text.length!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.housingTextField.text]]mutableCopy];
    }
    if (self.buildingTextField.text.length!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.buildingTextField.text]]mutableCopy];
    }
    
    [[LMGeocoder sharedInstance]geocodeAddressString:sentence service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
        if (results.count && !error) {
            LMAddress *address = [results firstObject];
            [address parseThoroughfare];
            if ([address isCorrectAddress]) {
                self.address = address;
                GMSCameraPosition *camera = [GMSCameraPosition
                                             cameraWithLatitude:self.address.coordinate.latitude
                                             longitude: self.address.coordinate.longitude
                                             zoom:self.mapView.camera.zoom];
                self.mapView.camera=camera;
                if (self.marker==nil) {
                    self.marker = [[GMSMarker alloc] init];
                    self.marker.position = self.address.coordinate;
                    if (self.isQuickOrder) {
                        self.marker.snippet = @"Тут";
                        [self.marker setDraggable: YES];
                    }else self.marker.snippet = @"Мне сюда";
                    self.marker.appearAnimation = kGMSMarkerAnimationPop;
                    self.marker.map = self.mapView;
                    self.marker.icon = [UIImage imageNamed:@"There"];
                } else {
                    self.marker.position = self.address.coordinate;
                }
                if (!self.isQuickOrder) {
                    [self createPath];
                }
            }
        }
    }];
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    if (self.marker==nil) {
        self.marker = [[GMSMarker alloc] init];
        self.marker.position = coordinate;
        if (self.isQuickOrder) {
            self.marker.snippet = @"Тут";
        }else self.marker.snippet = @"Мне сюда";
        self.marker.appearAnimation = kGMSMarkerAnimationPop;
        self.marker.map = self.mapView;
        self.marker.icon = [UIImage imageNamed:@"There"];
        
    } else self.marker.position = coordinate;
    self.address = nil;
    
    [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:coordinate                                                  service:kLMGeocoderGoogleService
                                        completionHandler:^(NSArray *results, NSError *error) {
                                            if (results.count && !error) {
                                                LMAddress *address = [results firstObject];
                                                self.address = address;
                                                [self.address parseThoroughfare];
                                                if (![NSString isEmpty:self.address.locality]) {
                                                    self.cityTextField.text = address.locality;
                                                }
                                                if (![NSString isEmpty:self.address.street]) {
                                                    self.streetTextField.text = address.street;
                                                }
                                                if (![NSString isEmpty:self.address.house]) {
                                                    self.homeTextField.text = address.house;
                                                }
                                                if (![NSString isEmpty:self.address.korpus]) {
                                                    self.housingTextField.text = address.korpus;
                                                }
                                                if (![NSString isEmpty:self.address.stroenie]) {
                                                    self.buildingTextField.text = address.stroenie;
                                                }
                                                if (!self.isQuickOrder) {
                                                    [self createPath];
                                                }
                                                if (self.mapViewTopMarginConstraint.constant==0) {
                                                    if (![NSString isEmpty:[self.address parseThoroughfare]]) {
                                                        [self.saveButton setTitle:[NSString stringWithFormat:@"%@ %@", self.address.locality, [self.address parseThoroughfare]] forState:UIControlStateNormal];
                                                        
                                                    } else [self.saveButton setTitle:self.address.locality forState:UIControlStateNormal];
                                                }
                                            }
                                        }];
}


-(void)createPath{
    LMAddress *addressFrom = ((RPTabBarViewController*)self.tabBarController).addressFrom;
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&units=metric&mode=driving&key=%@",addressFrom.coordinate.latitude,addressFrom.coordinate.longitude,self.address.coordinate.latitude,self.address.coordinate.longitude,[[NSUserDefaults standardUserDefaults]valueForKey:@"serverKey"]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            self.singleLine.map=nil;
            GMSPath *path =[GMSPath pathFromEncodedPath:responseObject[@"routes"][0][@"overview_polyline"][@"points"]];
            self.singleLine = [GMSPolyline polylineWithPath:path];
            self.singleLine.strokeWidth = 7;
            self.singleLine.strokeColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
            self.singleLine.map = self.mapView;
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}

- (IBAction)saveButtonPressed:(id)sender {
    if ([self.address isCorrectAddress]) {
        if (!self.isQuickOrder) {
            [((RPTabBarViewController*)self.tabBarController).addressDelegate passAddressTo:self.address];
        } else [self.addressDelegate passAddressTo:self.address];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)animateToMarkerPosition{
    [self.mapView animateToCameraPosition:[GMSCameraPosition
                                           cameraWithLatitude:self.marker.position.latitude
                                           longitude:self.marker.position.longitude
                                           zoom:self.mapView.camera.zoom]];
}

@end
