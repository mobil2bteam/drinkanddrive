//
//  RPOrderTabBarController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrderTabBarController.h"

@implementation RPOrderTabBarController
-(void)viewDidLoad{
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.547 green:1.0 blue:0.3281 alpha:1.0]];
}
@end
