//
//  RPOrderViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/18/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrderViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RPOrder.h"
#import "UIImageView+AFNetworking.h"
#import "RPServerManager.h"
#import "SCLAlertView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "RPCancelOrderWithRefuseAlertController.h"
#import "RPCancelOrderAlertController.h"

#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height
#define kAnimationDuration 1.0f
#define klocation  self.appDelegate.locationManager.location.coordinate

@interface RPOrderViewController()<UIGestureRecognizerDelegate,GMSMapViewDelegate,MBProgressHUDDelegate,RPUpdateLocation, RPCancelOrderProtocol, RPCancelOrderWithRefuseProtocol>
{
    MBProgressHUD *HUD;
}
@property (weak, nonatomic) IBOutlet UIImageView *animationImageView;
@property (weak, nonatomic) IBOutlet UIImageView *driverRunFontImageView;
@property (nonatomic, strong) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *starsImageViews;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *starsButtons;
@property (nonatomic, strong) GMSMarker *marker;

@property (weak, nonatomic) IBOutlet UILabel *driverRunStatusLabel;
// вьюхи
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *searchTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchBottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;
@property (weak, nonatomic) IBOutlet UIButton *operatorButton;
@property (weak, nonatomic) IBOutlet UIButton *operatorLastButton;
@property (weak, nonatomic) IBOutlet UIButton *waitMeButton;
@property (weak, nonatomic) IBOutlet UIButton *iAmReadyButton;
@property (weak, nonatomic) IBOutlet UIView *searchView;
//вью водителя
@property (weak, nonatomic) IBOutlet UIView *driverView;
@property (weak, nonatomic) IBOutlet UIButton *callDriverButton;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverSurnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
//вью с деталями заявки
@property (weak, nonatomic) IBOutlet UIView *orderInfoView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
// водитель ожидает
@property (weak, nonatomic) IBOutlet UIView *driverWaitYouView;
@property (weak, nonatomic) IBOutlet UILabel *driverWaitYouLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverWaitYouStatusLabel;
//вью с пунктами назначения
@property (weak, nonatomic) IBOutlet UIView *directionsBootomView;
@property (weak, nonatomic) IBOutlet UILabel *directionALabel;
@property (weak, nonatomic) IBOutlet UILabel *directionBLabel;
// вью со звездами
@property (weak, nonatomic) IBOutlet UIView *starsView;
@property (weak, nonatomic) IBOutlet UILabel *starsMessage2Label;
@property (weak, nonatomic) IBOutlet UILabel *starsMessage1Label;
@property (weak, nonatomic) IBOutlet UIButton *withoutOcenkaButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

// констрейнты
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *operatorButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *waitMeButtonLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iAmReadyButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverRunImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *withoutOcenkaLeadingConstraint;

// таймер для ложного статуса
@property (strong, nonatomic) NSTimer *timer;

// status index, need to remove
@property (nonatomic, assign) NSInteger ocenka;
@property (nonatomic, assign) NSInteger status;
@end

@implementation RPOrderViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.appDelegate.delegate=self;
    self.ocenka=5;
    self.navigationController.navigationBar.hidden=YES;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    self.driverView.clipsToBounds = YES;
    self.waitMeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.status=0;
    [self updateStatus];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate{
}

-(void)updateUserLocationWith:(CLLocation*)location{
    if ([RPOrder sharedOrder].status==5) {
        self.marker.position = location
        .coordinate;
        [self animateToMarkerPosition];
    }
}

// добавляет ложный статус водителя (Водитель выехал)
- (void)addFakeStatus{
    //если у данной заявки уже был ложный статус, то сразу устанавливаем водителя
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"%@key", [RPOrder sharedOrder].token];
    if ([userDefaults valueForKey:key]) {
        self.searchTopLabel.text = @"Водитель выехал...";
        self.searchBottomLabel.text = @"и свяжется с Вами в течении 5 минут";
        self.animationImageView.image = [UIImage imageNamed:@"driver_image"];
        return;
    }
    //если с заявкой пришла структура водителя, то устанавливаем ее, иначе запускаем анимацию поиска водителя
    if ([RPOrder sharedOrder].driver != nil) {
        self.searchTopLabel.text = @"Водитель выехал...";
        self.searchBottomLabel.text = @"и свяжется с Вами в течении 5 минут";
        self.animationImageView.image = [UIImage imageNamed:@"driver_image"];
    } else {
        [self start];
    }
    //запускаем таймер для ложного статуса (должен запустится через 5-10 секунд)
    if (self.timer == nil) {
        [self.timer invalidate];
        self.timer = nil;
        self.timer =  [NSTimer scheduledTimerWithTimeInterval:arc4random_uniform(5) + 6
                                                       target:self
                                                     selector:@selector(selectDriver)
                                                     userInfo:nil
                                                      repeats:NO];
    }
}

- (void)selectDriver{
    self.searchTopLabel.text = @"Водитель выехал...";
    self.searchBottomLabel.text = @"и свяжется с Вами в течении 5 минут";
    [self.animationImageView stopAnimating];
    self.animationImageView.image = [UIImage imageNamed:@"driver_image"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"%@key", [RPOrder sharedOrder].token];
    [userDefaults setValue:@(1) forKey:key];
    [userDefaults synchronize];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

-(void) willGetFocus {
    if ([RPOrder sharedOrder].status==1) {
        NSString *key = [NSString stringWithFormat:@"%@key", [RPOrder sharedOrder].token];
        if (![RPOrder sharedOrder].driver) {
            if (![[NSUserDefaults standardUserDefaults]valueForKey:key]) {
                [self.animationImageView startAnimating];
            }
        }
    }
    if ([RPOrder sharedOrder].status==3) [self.driverRunFontImageView startAnimating];
}

-(void)updateStatus{
    NSDictionary *param;
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"token2"]!=nil) {
        param = @{@"type":@"get",
                  @"userid":@([RPUser sharedUser].userid),
                  @"password":[RPUser sharedUser].password,
                  @"token":[[NSUserDefaults standardUserDefaults] valueForKey:@"token2"]};
    } else {
        param = @{@"type":@"get",
                  @"token":[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]};
    }
    [self addHudToView];
    [HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager]getOrder:param onSuccess:^(RPOrder *order, NSString *errorMessage) {
            if (errorMessage==nil) {
                [self getOrder:[RPOrder sharedOrder]];
            } else {
                //очищаем токены, если получили ошибку
                NSUserDefaults *userDefaults= [NSUserDefaults standardUserDefaults];
                if ([[NSUserDefaults standardUserDefaults]valueForKey:@"token2"]!=nil) {
                    if ([userDefaults valueForKey:@"token"]!=nil) {
                        [userDefaults removeObjectForKey:@"token"];
                    }
                    if ([userDefaults valueForKey:@"token2"]!=nil) {
                        [userDefaults removeObjectForKey:@"token2"];
                    }
                    [userDefaults synchronize];
                    if (self.navigationController) {
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                } else {
                    if ([userDefaults valueForKey:@"token"]!=nil) {
                        [userDefaults removeObjectForKey:@"token"];
                    }
                    [userDefaults synchronize];
                    [self dismissViewControllerAnimated:NO completion:nil];
                }
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            if (self.navigationController) {
                [self.navigationController popViewControllerAnimated:NO];
            } else {
                [self dismissViewControllerAnimated:NO completion:nil];
            }
        }];
    }];
}

-(void)start{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 19; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
        if (i==1 || i==19) {
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
        }
    }
    
    for (int i = 12; i >= 1; i--) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
        if (i==1) {
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
        }
    }

    for (int i = 32; i <= 49; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
    }

    for (int i = 42; i >= 32; i--) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"anim1_%d.png", i]]];
    }

    self.animationImageView.animationImages = images;
    self.animationImageView.animationDuration = images.count/8.f;
    self.animationImageView.animationRepeatCount = 0;
    [self.animationImageView startAnimating];
}

-(void)startDriverAnimation{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    
    for (int i = 1; i <= 6; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
        if (i==4) {
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
        }
        if (i==5) {
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"falling_0000%d.png", i]]];
        }
    }
    
    for (NSInteger i=0; i<5; i++) {
        for (int i = 3; i <= 8; i++) {
            [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"run_0000%d.png", i]]];
        }
    }
    self.driverRunFontImageView.animationImages = images;
    self.driverRunFontImageView.animationDuration = images.count/8.f;
    self.driverRunFontImageView.animationRepeatCount = 0;
    [self.driverRunFontImageView startAnimating];
}

#pragma mark GMSMapView Delegate
- (void)mapView:(GMSMapView *)mapView    didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    if (self.status==3) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            self.mapViewTopConstraint.constant = self.mapViewTopConstraint.constant == 0?(kScreenHeight-20)/2-50 : 0;
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - Actions

-(void)getOrder:(RPOrder *)order{
    if (order.status==self.status+1 || ((order.status==10 || order.status==11) && self.status==4)) {
    self.status = order.status;
    [[NSUserDefaults standardUserDefaults]setValue:@(self.status) forKey:@"status"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    switch ([RPOrder sharedOrder].status) {
        case 1:
        {
            [self addFakeStatus];
            self.animationImageView.hidden=self.cancelOrderButton.hidden=self.operatorButton.hidden=self.searchView.hidden=NO;
            [UIView animateWithDuration:kAnimationDuration animations:^{
            self.searchViewTopConstraint.constant=0;
            self.animationImageView.alpha=1;
            [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                [UIView animateWithDuration:kAnimationDuration
                                      delay:0
                     usingSpringWithDamping:0.6
                      initialSpringVelocity:0.5
                                    options:0
                                 animations:^{
                                     self.cancelButtonLeadingConstraint.constant=self.operatorButtonTrailingConstraint.constant=0;
                                     [self.view layoutIfNeeded];
                                 } completion:nil];
            }];
        }
            break;
        case 2:
        {
            [self.animationImageView stopAnimating];
            self.animationImageView.hidden=YES;
            self.orderInfoView.alpha=0.f;
            self.orderInfoView.hidden=NO;
            self.driverRunImageViewHeightConstraint.constant = (kScreenHeight-20)/2-50;
                        if ([RPOrder sharedOrder].driver!=nil) {
                            [self fillDriverInfoView:order];
                        }
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.backgroundView.alpha=0.9f;
                self.searchView.alpha=0;
                self.orderInfoView.alpha=1;
                self.dateLabel.text = [RPOrder sharedOrder].date;
                self.timeLabel.text = [RPOrder sharedOrder].time;
                
            }completion:^(BOOL finished) {
                // показываем водилеля
                self.driverView.hidden = NO;
                                if ([RPOrder sharedOrder].driver!=nil) {
                [UIView animateWithDuration:kAnimationDuration animations:^{
                    self.driverViewHeightConstraint.constant=100;
                    [self.view layoutIfNeeded];
                }];
                                }
            }];
        }
            break;
        case 3:
        {
            [self startDriverAnimation];
            [self addDriverMarker:order];
            self.driverRunStatusLabel.text = @"Водитель торопиться к вам";
            self.driverRunStatusLabel.hidden=NO;
            self.orderInfoView.hidden=YES;
            self.callDriverButton.hidden=NO;
                        [self addDriverMarker:order];
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.mapViewTopConstraint.constant = self.mapViewBottomConstraint.constant =  (kScreenHeight-120)/2;
                                [self.mapView animateToCameraPosition:[GMSCameraPosition
                                                                       cameraWithLatitude:[RPOrder sharedOrder].driver.lan
                                                                       longitude:[RPOrder sharedOrder].driver.lon
                                                                       zoom:17]];
                                self.driverViewTopConstraint.constant = (kScreenHeight-20)/2+50;
                [self.view layoutIfNeeded];
                self.backgroundView.alpha=0;
            }];
            
        }
            break;
        case 4:
        {
            [self.driverRunFontImageView stopAnimating];
            self.driverRunStatusLabel.hidden=YES;
            self.driverWaitYouView.alpha=0;
            self.driverWaitYouView.hidden=NO;
            self.cancelOrderButton.hidden=self.operatorButton.hidden=YES;
            self.waitMeButton.hidden=self.iAmReadyButton.hidden=NO;
            self.backgroundView.hidden=NO;
            self.callDriverButton.hidden=YES;
                        self.driverWaitYouLabel.text = [RPOrder sharedOrder].messageText1;
                        self.driverWaitYouStatusLabel.text= [RPOrder sharedOrder].statusText;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.iAmReadyButtonTrailingConstraint.constant = self.waitMeButtonLeadingConstraint.constant=0;
                self.mapViewTopConstraint.constant = self.mapViewBottomConstraint.constant = 0;
                self.driverViewTopConstraint.constant = 40;
                self.driverWaitYouView.alpha=1.f;
                [self.view layoutIfNeeded];
                self.backgroundView.alpha=0.9;
            }];
            
        }
            break;
            
        case 6:
        {
            self.operatorLastButton.hidden=YES;
            self.directionsBootomView.hidden = YES;
            self.starsView.alpha=0;
            self.starsMessage1Label.alpha=0;
            self.starsMessage2Label.alpha=0;
            self.starsView.hidden=NO;
            self.starsMessage1Label.hidden=self.starsMessage2Label.hidden=NO;
            self.starsMessage1Label.hidden=self.starsMessage2Label.hidden=NO;
            self.waitMeButton.hidden=self.iAmReadyButton.hidden=YES;
            self.withoutOcenkaButton.hidden=self.okButton.hidden=NO;
            self.starsMessage1Label.text = [RPOrder sharedOrder].messageText1;
            self.starsMessage2Label.text = [RPOrder sharedOrder].messageText2;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.starsView.alpha=1;
                self.starsMessage1Label.alpha=1;
                self.starsMessage2Label.alpha=1;
                self.withoutOcenkaLeadingConstraint.constant = self.okButtonTrailingConstraint.
                constant=self.mapViewTopConstraint.constant=self.mapViewBottomConstraint.constant=0;
                self.backgroundView.hidden=NO;
                self.backgroundView.alpha=0.95;
                [self.view layoutIfNeeded];
            }];
        }
            break;
            
        case 5:
        {
            [self fillDirectionsAddresses:order];
            self.marker.map=nil;
            self.marker = [[GMSMarker alloc] init];
            self.marker.appearAnimation = kGMSMarkerAnimationPop;
            self.marker.icon = [UIImage imageNamed:@"CarDark"];
            self.marker.position = CLLocationCoordinate2DMake(klocation.latitude,klocation.longitude);
            self.marker.map=self.mapView;
            [self animateToMarkerPosition];
            self.directionsBootomView.alpha=0;
            self.directionsBootomView.hidden = NO;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.driverViewTopConstraint.constant = 0;
                self.driverWaitYouView.alpha=0;
                self.backgroundView.alpha=0;
                self.mapViewTopConstraint.constant=100;
                self.mapViewBottomConstraint.constant=126;
                self.directionsBootomView.alpha=1;
                [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                self.driverWaitYouView.hidden=0;
                self.operatorLastButton.hidden=YES;
                self.backgroundView.hidden=YES;
            }];
        }
            break;
        case 10:
        {
            self.waitMeButton.hidden=YES;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.iAmReadyButtonTrailingConstraint.constant = kScreenWidth/4;
                self.iAmReadyButton.transform = CGAffineTransformMakeScale(1.3, 1.3);
                [self.view layoutIfNeeded];
            }];
            
        }
            break;
        case 11:
        {
            self.waitMeButton.hidden=YES;
            self.iAmReadyButton.hidden=YES;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.iAmReadyButton.alpha=0;
            }completion:^(BOOL finished) {
                self.iAmReadyButton.hidden=YES;
                self.iAmReadyButton.alpha=1.f;
                self.operatorButtonTrailingConstraint.constant = kScreenWidth/4;
                self.operatorLastButton.hidden = NO;
            }];
        }
            break;
            
        default:
            break;
    }
    } else {
        [self prepareViewForStatus:order];
    }
}

-(void)fillDriverInfoView:(RPOrder *)order{
    self.driverImageView.image = nil;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[RPOptions sharedOptions].url_suite,[RPOrder sharedOrder].driver.foto]];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.driverImageView setImageWithURLRequest:imageRequest
                                    placeholderImage:nil
                                         success:nil
                                             failure:nil];
    NSString *surname = [[RPOrder sharedOrder].driver.name componentsSeparatedByString:@" "][0];
    self.driverSurnameLabel.text =surname;
    self.driverNameLabel.text = [[RPOrder sharedOrder].driver.name substringFromIndex:surname.length+1];
    for (NSInteger i=0; i<[RPOrder sharedOrder].driver.reting; i++) {
        UIImageView *starImageVeiw = self.starsImageViews[i];
        starImageVeiw.image = [UIImage imageNamed:@"Star"];
    }
    for (NSInteger i=[RPOrder sharedOrder].driver.reting; i<self.starsImageViews.count; i++) {
        UIImageView *starImageVeiw = self.starsImageViews[i];
        starImageVeiw.image = [UIImage imageNamed:@"StarNonActive"];
    }

}

-(void)fillDirectionsAddresses:(RPOrder*)order{
    if ([RPOrder sharedOrder].wayTo.city.length!=0) {
        NSString *toAddress = [NSString stringWithFormat:@"%@, %@, %@",[RPOrder sharedOrder].wayTo.city,[RPOrder sharedOrder].wayTo.street,[RPOrder sharedOrder].wayTo.house];
        if ([RPOrder sharedOrder].wayTo.korpus.length!=0) {
            toAddress = [toAddress stringByAppendingString:[NSString stringWithFormat: @", корпус %@",[RPOrder sharedOrder].wayTo.korpus]];
        }
        if ([RPOrder sharedOrder].wayTo.stroenie.length!=0) {
            toAddress = [toAddress stringByAppendingString:[NSString stringWithFormat: @", строение %@",[RPOrder sharedOrder].wayTo.stroenie]];
        }
        self.directionBLabel.text = toAddress;
    }
    
    NSString *fromAddress = [NSString stringWithFormat:@"%@, %@, %@",[RPOrder sharedOrder].wayFrom.city,[RPOrder sharedOrder].wayFrom.street,[RPOrder sharedOrder].wayFrom.house];
    if ([RPOrder sharedOrder].wayFrom.korpus.length!=0) {
        fromAddress = [fromAddress stringByAppendingString:[NSString stringWithFormat: @", корпус %@",[RPOrder sharedOrder].wayFrom.korpus]];
    }
    if ([RPOrder sharedOrder].wayFrom.stroenie.length!=0) {
        fromAddress = [fromAddress stringByAppendingString:[NSString stringWithFormat: @", строение %@",[RPOrder sharedOrder].wayFrom.stroenie]];
    }
    self.directionALabel.text = fromAddress;
    
}

-(void)addDriverMarker:(RPOrder*)order{
    if (self.marker==nil) {
        self.marker = [[GMSMarker alloc] init];
        self.marker.position = CLLocationCoordinate2DMake([RPOrder sharedOrder].driver.lan,[RPOrder sharedOrder].driver.lon);
        self.marker.appearAnimation = kGMSMarkerAnimationPop;
        self.marker.icon = [UIImage imageNamed:@"Driver"];
        self.marker.map=self.mapView;
    } else self.marker.position = CLLocationCoordinate2DMake([RPOrder sharedOrder].driver.lan,[RPOrder sharedOrder].driver.lon);
}

-(void)prepareViewForStatus:(RPOrder*)order{
    [self.animationImageView stopAnimating];
    [self.driverImageView stopAnimating];
    self.animationImageView.hidden=YES;
    self.driverViewTopConstraint.constant=20;
    self.marker.map=nil;
    self.marker=nil;
    self.iAmReadyButton.hidden=self.okButton.hidden=self.waitMeButton.hidden=self.operatorLastButton.hidden=self.withoutOcenkaButton.hidden=self.cancelOrderButton.hidden=self.operatorButton.hidden=YES;
    self.searchView.hidden=self.driverView.hidden=self.orderInfoView.hidden=self.driverWaitYouView.hidden=YES;
    self.starsView.hidden=self.starsMessage1Label.hidden=self.starsMessage2Label.hidden=self.directionsBootomView.hidden=YES;
    self.backgroundView.alpha=1.f;
    self.backgroundView.hidden=NO;
    NSInteger status = [RPOrder sharedOrder].status;
    self.status = status;
    if (status==1) {
        self.status = status;
        self.searchView.hidden=NO;
        self.searchViewTopConstraint.constant=self.cancelButtonLeadingConstraint.constant=self.operatorButtonTrailingConstraint.constant=0;
        self.animationImageView.hidden=NO;
        [self addFakeStatus];
    }
    if (status==2) {
        self.cancelButtonLeadingConstraint.constant=self.operatorButtonTrailingConstraint.constant=0;
        self.orderInfoView.hidden=NO;
        self.driverRunImageViewHeightConstraint.constant = (kScreenHeight-20)/2-50;
        self.backgroundView.alpha=0.9f;
        self.dateLabel.text = [RPOrder sharedOrder].date;
        self.timeLabel.text = [RPOrder sharedOrder].time;
    }
    if (status==3) {
        self.driverRunImageViewHeightConstraint.constant = (kScreenHeight-20)/2-50;
        self.cancelButtonLeadingConstraint.constant=self.operatorButtonTrailingConstraint.constant=0;
        self.driverViewTopConstraint.constant = (kScreenHeight-20)/2+50;
        self.callDriverButton.hidden=NO;
        self.driverRunStatusLabel.text = @"Водитель торопиться к вам";
        self.driverRunStatusLabel.hidden=NO;
        [self startDriverAnimation];
        [self addDriverMarker:order];
        self.mapViewTopConstraint.constant = self.mapViewBottomConstraint.constant =  (kScreenHeight-120)/2;
        [self.mapView animateToCameraPosition:[GMSCameraPosition
                                               cameraWithLatitude:[RPOrder sharedOrder].driver.lan
                                               longitude:[RPOrder sharedOrder].driver.lon
                                               zoom:17]];
        self.driverViewTopConstraint.constant = (kScreenHeight-20)/2+50;
        self.backgroundView.alpha=0;
    }

    if (status==4 || status==10 || status==11) {
        self.driverViewTopConstraint.constant = (kScreenHeight-20)/2+50;
        self.driverWaitYouView.hidden=NO;
        self.cancelOrderButton.hidden=self.operatorButton.hidden=YES;
        self.waitMeButton.hidden=self.iAmReadyButton.hidden=NO;
        self.backgroundView.hidden=NO;
        self.callDriverButton.hidden=YES;
        self.driverWaitYouLabel.text = [RPOrder sharedOrder].messageText1;
        self.driverWaitYouStatusLabel.text= [RPOrder sharedOrder].statusText;
        self.iAmReadyButtonTrailingConstraint.constant = self.waitMeButtonLeadingConstraint.constant=self.mapViewTopConstraint.constant = self.mapViewBottomConstraint.constant = 0;
        self.driverViewTopConstraint.constant = 40;
        self.driverWaitYouView.alpha=1.f;
        self.backgroundView.alpha=0.9;
        if (status==10) {
            self.waitMeButton.hidden=YES;
            self.iAmReadyButtonTrailingConstraint.constant = kScreenWidth/4;
            self.iAmReadyButton.transform = CGAffineTransformMakeScale(1.3, 1.3);
        }
        if (status==11) {
            self.waitMeButton.hidden=YES;
            self.iAmReadyButton.hidden=YES;
            self.operatorButtonTrailingConstraint.constant = kScreenWidth/4;
            self.operatorLastButton.hidden = NO;
        }
    }
    
    if (status==5) {
        [self fillDirectionsAddresses:order];
        self.driverViewTopConstraint.constant = 0;
        self.marker = [[GMSMarker alloc] init];
        self.marker.appearAnimation = kGMSMarkerAnimationPop;
        self.marker.icon = [UIImage imageNamed:@"CarDark"];
        self.marker.position = CLLocationCoordinate2DMake(klocation.latitude,klocation.longitude);
        self.marker.map=self.mapView;
        [self animateToMarkerPosition];
        self.directionsBootomView.hidden = NO;
        self.mapViewTopConstraint.constant=100;
        self.mapViewBottomConstraint.constant=126;
        self.operatorLastButton.hidden=YES;
        self.backgroundView.hidden=YES;
    }
    if (status==6) {
        [self fillDirectionsAddresses:order];
        self.driverViewTopConstraint.constant = 0;
        self.starsView.hidden=self.starsMessage1Label.hidden=self.starsMessage2Label.hidden=self.withoutOcenkaButton.hidden=self.okButton.hidden=NO;
        self.starsMessage1Label.text = [RPOrder sharedOrder].messageText1;
        self.starsMessage2Label.text = [RPOrder sharedOrder].messageText2;
        self.withoutOcenkaLeadingConstraint.constant = self.okButtonTrailingConstraint.constant=self.mapViewTopConstraint.constant=self.mapViewBottomConstraint.constant=0;
        self.backgroundView.hidden=NO;
        self.backgroundView.alpha=0.95;
    }
    
    if (status>=2) {
        if ([RPOrder sharedOrder].driver!=nil) {
            self.driverViewHeightConstraint.constant=100;
            [self fillDriverInfoView:order];
        }
        else self.driverViewHeightConstraint=0;
        self.driverView.hidden=NO;
    }
    if (status<=3) {
        self.cancelOrderButton.hidden=self.operatorButton.hidden=NO;
    }
    [self.view layoutIfNeeded];
}

-(void)addHudToView{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = NO;
    HUD.delegate = self;
    HUD.labelText = @"Загрузка...";
}

-(void)sendMessageForDriver:(NSInteger)value{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    // 1 - i go
    // 2 - wait me
    NSDictionary *param;
    if ([userDefaults valueForKey:@"token2"]!=nil) {
        param =@{@"type":@"message_for_driver",
                 @"userid":@([RPUser sharedUser].userid),
                 @"password":[RPUser sharedUser].password,
                 @"token":[RPOrder sharedOrder].token,
                 @"value":@(value)
                 };
    } else param = @{@"type":@"message_for_driver",
                     @"token":[RPOrder sharedOrder].token,
                     @"value":@(value)
                     };
    [self addHudToView];
    [HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager]sendMessageForDriver:param onSuccess:^(NSString *errorMessage) {
            if (errorMessage==nil) {
                if ([RPOrder sharedOrder].status==10) {
                    self.waitMeButton.hidden=YES;
                    [UIView animateWithDuration:kAnimationDuration animations:^{
                        self.iAmReadyButtonTrailingConstraint.constant = kScreenWidth/4;
                        self.iAmReadyButton.transform = CGAffineTransformMakeScale(1.3, 1.3);
                        [self.view layoutIfNeeded];
                    }];
                } else if  ([RPOrder sharedOrder].status==11) {
                    self.waitMeButton.hidden=YES;
                    self.iAmReadyButton.hidden=YES;
                    [UIView animateWithDuration:kAnimationDuration animations:^{
                        self.iAmReadyButton.alpha=0;
                    }completion:^(BOOL finished) {
                        self.iAmReadyButton.hidden=YES;
                        self.iAmReadyButton.alpha=1.f;
                        self.operatorButtonTrailingConstraint.constant = kScreenWidth/4;
                        self.operatorLastButton.hidden = NO;
                    }];
                }
            } else {
                [self showErrorMessage:errorMessage];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
}

-(void)closeOrderWithOcenka:(NSInteger)value{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *param;
    if ([userDefaults valueForKey:@"token2"]!=nil) {
        param = @{@"type":@"close",
                  @"userid":@([RPUser sharedUser].userid),
                  @"password":[RPUser sharedUser].password,
                  @"token":[RPOrder sharedOrder].token,
                  @"ocenka":@(value)
                  };
        
    } else param = @{@"type":@"close",
                     @"token":[RPOrder sharedOrder].token,
                     @"ocenka":@(value)
                     };

    
    [self addHudToView];
    self.view.userInteractionEnabled = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager]closeOrderWithOcenka:param onSuccess:^(NSString *errorMessage) {
            self.view.userInteractionEnabled = YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage==nil) {
                if ([userDefaults valueForKey:@"token2"]!=nil) {
                    [self.navigationController popViewControllerAnimated:NO];
                    if ([userDefaults valueForKey:@"token2"]!=nil) {
                        [userDefaults removeObjectForKey:@"token2"];
                    }
                    [userDefaults synchronize];
                } else {
                    [self dismissViewControllerAnimated:NO completion:nil];
                    if ([userDefaults valueForKey:@"token"]!=nil) {
                        [userDefaults removeObjectForKey:@"token"];
                    }
                    [userDefaults synchronize];
                }
                
            } else {
                [self showErrorMessage:errorMessage];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            self.view.userInteractionEnabled = YES;;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];

}

-(void)animateToMarkerPosition{
    [self.mapView animateToCameraPosition:[GMSCameraPosition
                                           cameraWithLatitude:self.marker.position.latitude
                                           longitude:self.marker.position.longitude
                                           zoom:17]];
}

#pragma mark - RPCancelOrderWithRefuseProtocol

- (void)cancelOrderWithRefuseID:(NSInteger)refuseID{
    NSMutableDictionary *param = [@{@"type":@"cancel",
                                    @"token":[RPOrder sharedOrder].token,
                                    @"id_cancel":@(refuseID)
                                    }mutableCopy];
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"token2"]!=nil) {
        param[@"userid"]=@([RPUser sharedUser].userid);
        param[@"password"]=[RPUser sharedUser].password;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.view.userInteractionEnabled = NO;
    [[RPServerManager sharedManager]cancelOrder:param onSuccess:^(NSString *errorMessage) {
        self.view.userInteractionEnabled = YES;
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (errorMessage==nil) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"token2"]!=nil) {
                if ([userDefaults valueForKey:@"token"]!=nil) {
                    [userDefaults removeObjectForKey:@"token"];
                }
                if ([userDefaults valueForKey:@"token2"]!=nil) {
                    [userDefaults removeObjectForKey:@"token2"];
                }
                [userDefaults synchronize];
                [self.navigationController popViewControllerAnimated:NO];
            } else {
                if ([userDefaults valueForKey:@"token"]!=nil) {
                    [userDefaults removeObjectForKey:@"token"];
                }
                [userDefaults synchronize];
                [self dismissViewControllerAnimated:NO completion:nil];
            }
        } else {
            [self showErrorMessage:errorMessage];
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        self.view.userInteractionEnabled = YES;;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self showErrorMessage:@"Произошла ошибка"];
    }];
}

#pragma mark - RPCancelOrderProtocol

- (void)cancelOrder{
    NSArray* words = [[RPOptions sharedOptions].operatorPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

#pragma mark - Actions
- (IBAction)callDriverButtonPressed:(id)sender {
    NSArray* words = [[RPOrder sharedOrder].driver.phone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}
- (IBAction)cancelButonPressed:(id)sender {
    
    // если заявка в статусе меньше чем "Водитель вышел", то предлагаем выбрать одну из причин отмены
    if ([RPOrder sharedOrder].status < 3) {
        RPCancelOrderWithRefuseAlertController *cancelOrderVC = [[RPCancelOrderWithRefuseAlertController alloc] initWithNibName: @"RPCancelOrderWithRefuseAlertController" bundle: nil];
        cancelOrderVC.delegate = self;
        cancelOrderVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        cancelOrderVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:cancelOrderVC animated:YES completion:nil];
    } else {
        // иначе заявка отменяется только по телефону через оператора
        RPCancelOrderAlertController *cancelOrderVC = [[RPCancelOrderAlertController alloc] initWithNibName: @"RPCancelOrderAlertController" bundle: nil];
        cancelOrderVC.delegate = self;
        cancelOrderVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        cancelOrderVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:cancelOrderVC animated:YES completion:nil];
    }
}

- (IBAction)operatorButtonPressed:(id)sender {
    NSArray* words = [[RPOptions sharedOptions].operatorPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)closeWithoutOcenkaButtonPressed:(id)sender {
    [self closeOrderWithOcenka:0];
}

- (IBAction)closeWithOcenkaButtonPressed:(id)sender {
    [self closeOrderWithOcenka:self.ocenka];
}

- (IBAction)waitMeButtonPressed:(id)sender {
    [self sendMessageForDriver:2];
}

- (IBAction)iAmReadyButtonPressed:(id)sender {
    [self sendMessageForDriver:1];
}

- (IBAction)operatorLastButtonPressed:(id)sender {
    NSArray* words = [[RPOptions sharedOptions].operatorPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)starButtonPressed:(id)sender {
    self.ocenka = ((UIButton*)sender).tag + 1;
    for (NSInteger i=0; i<self.starsButtons.count; i++) {
        UIButton *button = (UIButton*)self.starsButtons[i];
        [UIView animateWithDuration:0.7 animations:^{
            if (i<=((UIButton*)sender).tag) {
                button.alpha=1.f;
            } else button.alpha=0.1f;
        }];
    }
}

@end
