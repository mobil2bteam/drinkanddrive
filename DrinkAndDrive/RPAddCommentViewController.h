//
//  RPAddCommentViewController.h
//  DrinkNDrive
//
//  Created by Ruslan on 4/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPAdditionalDataViewController.h"

@interface RPAddCommentViewController : UIViewController

@property (nonatomic, strong) id <RPAddCommentProtocol> delegate;

@property (nonatomic, strong) NSString *comment;

@end
