//
//  RPSearchListViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPSearchListViewController.h"
#import "MBProgressHUD.h"
#import "RPAddress.h"
#import "RPServerManager.h"
#import "RPAddressTableViewCell.h"
#import "SCLAlertView.h"
#import "RPTabBarViewController.h"
@interface RPSearchListViewController()<UITableViewDelegate, UITableViewDataSource,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<RPAddress*> *addressArray;

@end
@implementation RPSearchListViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self loadAddresses];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPAddressTableViewCell *cell = (RPAddressTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    RPAddress *address = self.addressArray[indexPath.row];
    cell.cityLabel.text = address.city;
    cell.streetLabel.text = address.street;
    cell.houseLabel.text = [NSString stringWithFormat:@"Дом:%@", address.house];
    if (address.korpus!=0) {
        cell.housingLabel.text = [NSString stringWithFormat:@"Корп.:%ld", (long)address.korpus];
    }
    if (address.stroenie!=0) {
        cell.buildingLabel.text = [NSString stringWithFormat:@"Стр.:%ld", (long)address.stroenie];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPAddress *address = self.addressArray[indexPath.row];
    
    NSMutableString *sentence = [[NSString stringWithFormat:@"Россия,%@,%@,%@",address.city,address.street,address.house]mutableCopy];
    if (address.korpus!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%ld",(long)address.korpus]]mutableCopy];
    }
    if (address.stroenie!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%ld",(long)address.stroenie]]mutableCopy];
    }
    
    [[LMGeocoder sharedInstance]geocodeAddressString:sentence service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
        if (results.count && !error) {
            LMAddress *address = [results firstObject];
            [((RPTabBarViewController*)self.tabBarController).addressDelegate passAddressTo:address];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    
}

-(void)loadAddresses{
    NSDictionary *params = @{@"type":@"addresslist",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getAddressesWithParameters:params onSuccess:^(NSMutableArray <RPAddress*> *addresses, NSString *errorMessage) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                _addressArray = addresses;
                [_tableView reloadData];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка!"];
        }];
    });
}
@end
