//
//  RPCarListViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/3/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPAddressListViewController.h"
#import "RPEditAddressViewController.h"
#import "RPAddressTableViewCell.h"
#import "Helper.h"

@interface RPAddressListViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *addAddressLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<RPAddress*> *addressArray;
@end
@implementation RPAddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadAddresses];
    [self.addAddressLabel setHidden:YES];
    // Do any additional setup after loading the view.
}


-(void)reload{
    [self loadAddresses];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPAddressTableViewCell *cell = (RPAddressTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    RPAddress *address = self.addressArray[indexPath.row];
    cell.cityLabel.text = address.city;
    cell.streetLabel.text = address.street;
    cell.houseLabel.text = [NSString stringWithFormat:@"Дом:%@", address.house];
    cell.housingLabel.text = [NSString stringWithFormat:@"Корп.:"];
    cell.buildingLabel.text =  [NSString stringWithFormat:@"Стр.:"];
    if (address.korpus!=0) {
        cell.housingLabel.text = [NSString stringWithFormat:@"Корп.:%ld", (long)address.korpus];
    }
    if (address.stroenie!=0) {
        cell.buildingLabel.text = [NSString stringWithFormat:@"Корп.:%ld", (long)address.stroenie];
    }
    return cell;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    RPEditAddressViewController *newController = [segue destinationViewController];
    newController.delegate = self;
    if ([segue.identifier isEqualToString:@"addAddress"])
    {
        newController.isEditAddressMode = NO;
        
    } else {
        newController.isEditAddressMode = YES;
        RPAddress *address =self.addressArray[[self.tableView indexPathForSelectedRow].row];
        newController.addressId = @(address.addressId);
    }
    
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"addAddress"]) {
        return YES;
    }
    if ([identifier isEqualToString:@"editAddress"]) {
        return YES;
    }
    return NO;
}


-(void)loadAddresses{
    
    NSDictionary *params = @{@"type":@"addresslist",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [[RPServerManager sharedManager] getAddressesWithParameters:params onSuccess:^(NSMutableArray <RPAddress*> *addresses, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                _addressArray = addresses;
                [_tableView reloadData];
            }
            if (_addressArray.count==0) {
                [_addAddressLabel setHidden:NO];
                [_tableView setHidden:YES];
            } else {
                [_addAddressLabel setHidden:YES];
                [_tableView setHidden:NO];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
            self.addAddressLabel.hidden=NO;
        }];
    });

}
@end
