//
//  RPInviteTableViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPInviteViewController : UITableViewController
- (IBAction)menuButtonTapped:(id)sender;
@property (nonatomic, assign) BOOL needHiddenMenuButton;;
@end
