//
//  RPOrderTabBarController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderTabBarController : UITabBarController
@property (nonatomic, strong) NSString  *token;
@end
