//
//  RPAdditionalDataViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 1/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAdditionalDataViewController.h"
#import "RPTabBarViewController.h"
#import "RPAddCommentViewController.h"

#define kScreenWidth [[UIScreen mainScreen] bounds].size.width

@interface RPAdditionalDataViewController ()<ARSPopoverDelegate,UIPickerViewDataSource, UIPickerViewDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (strong) ARSPopover *popover;
@property (weak, nonatomic) IBOutlet UIButton *changeDateButton;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSMutableArray<RPCar*> *carsArray;
@property (weak, nonatomic) IBOutlet UISwitch *balSwitch;
@property (weak, nonatomic) IBOutlet UILabel *balLabel;
@property (weak, nonatomic) IBOutlet UIButton *addCommentButton;
@property (weak, nonatomic) IBOutlet UIButton *selectedCarButton;
@property (weak, nonatomic) IBOutlet UIButton *carOpenList;
@property (nonatomic, assign) NSInteger selectedCarIndex;
@property (nonatomic, strong) NSString *addressToParse;
@property (nonatomic, strong) NSString *comment;

#pragma mark - address from
@property (nonatomic, strong) LMAddress *addressTo;

#pragma mark - address to
@property (weak, nonatomic) IBOutlet UIButton *sayDriverButton;
//@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
//@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end

@implementation RPAdditionalDataViewController

#pragma mark - Lifecycle
-(void)viewDidLoad
{
    [super viewDidLoad];
    self.comment = @"";
    self.addCommentButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.addCommentButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.addCommentButton.titleLabel.numberOfLines = 2;
    [self.balSwitch setOn:NO];
    self.selectedCarIndex=-1;
    [self loadCars];
    if ([RPUser sharedUser].ball == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.balLabel removeFromSuperview];
            [self.balSwitch removeFromSuperview];
            [self.view layoutIfNeeded];
        });
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=NO;
    
}

//- (void)searchDistance{
//    
//    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&units=metric&mode=driving&key=%@",self.addressFrom.coordinate.latitude,self.addressFrom.coordinate.longitude,self.addressTo.coordinate.latitude, self.addressTo.coordinate.longitude, [[NSUserDefaults standardUserDefaults]valueForKey:@"serverKey"]];
//    NSURL *wurl = [NSURL URLWithString:url];
//    NSData *data = [NSData dataWithContentsOfURL: wurl];
//    
//    if (nil == data) {
//    }
//    else{
//        NSError *error;
//        NSDictionary *json = [NSJSONSerialization
//                              JSONObjectWithData:data
//                              options:kNilOptions
//                              error:&error];
//        
//        NSString *resultStatus = [json valueForKey:@"status"];
//        if ( (nil == error) && [resultStatus isEqualToString:@"OK"] ) {
//            NSString *distance = [[[[json valueForKeyPath:@"routes.legs.distance"]firstObject]valueForKey:@"text"]firstObject];
//            NSString *duration = [[[[json valueForKeyPath:@"routes.legs.duration"]firstObject]valueForKey:@"text"]firstObject];
//            self.distanceLabel.text = [NSString stringWithFormat:@"Расстояние : %@",distance];
//            self.durationLabel.text = [NSString stringWithFormat:@"Время в пути : %@",duration];
//        }
//        
//    }
//    
//}

#pragma mark - Methods
-(void)checkCars{
    for (NSInteger i=0; i<self.carsArray.count; i++) {
        RPCar *car = self.carsArray[i];
        if (car.def==1) {
            self.selectedCarIndex= i;
            [self.selectedCarButton setTitle:[NSString stringWithFormat:@"%@(%@)", car.marka,car.numberCar] forState:UIControlStateNormal];
            return;
        }
    }
    RPCar *car = self.carsArray[0];
    self.selectedCarIndex= 0;
    [self.selectedCarButton setTitle:[NSString stringWithFormat:@"%@(%@)", car.marka,car.numberCar] forState:UIControlStateNormal];
}
-(void)loadCars{
    NSDictionary *params = @{@"type":@"carlist",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getCarsWithParameters:params onSuccess:^(NSMutableArray <RPCar*> *cars, NSString *errorMessage) {
            [self.backgroundView removeFromSuperview];
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
            } else {
                self.carsArray = cars;
                if (cars.count!=0) {
                    [self checkCars];
                } else {
                    [self.selectedCarButton removeFromSuperview];
                    [self.carOpenList removeFromSuperview];
                }
            }
            
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self.backgroundView removeFromSuperview];
        }];
    });
}


#pragma mark  - <ARSPopoverDelegate>
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    if (self.popover.sourceView == self.carOpenList) {
        NSString *string = [NSString stringWithFormat:@"%@(%@)",((RPCar*)self.carsArray[self.selectedCarIndex]).marka,((RPCar*)self.carsArray[self.selectedCarIndex]).numberCar];
        [self.selectedCarButton setTitle:string forState:UIControlStateNormal];
    } else {
        if (self.selectedDate!=nil) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.timeStyle = kCFDateFormatterShortStyle;
            dateFormatter.dateStyle = kCFDateFormatterShortStyle;
            NSDate *date = self.selectedDate;
            NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
            [dateFormatter setLocale:ruLocale];
            [self.changeDateButton setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
        }
    }
}
- (IBAction)clearDatePressed:(id)sender {
    self.selectedDate = nil;
    [self.changeDateButton setTitle:@"Как можно быстрее..." forState:UIControlStateNormal];
}

- (IBAction)clearLocationPressed:(id)sender {
    [self.sayDriverButton setTitle:@"Куда : Скажу водителю" forState:UIControlStateNormal];
    self.addressTo=nil;
}

- (IBAction)clearCommentButtonPressed:(id)sender {
    self.comment = @"";
    [self.addCommentButton setTitle:@"Добавить комментарий" forState:UIControlStateNormal];
}

- (IBAction)addCommentButtonPressed:(id)sender {
    RPAddCommentViewController *newController = [[RPAddCommentViewController alloc] initWithNibName: @"RPAddCommentViewController" bundle: nil];
    newController.delegate = self;
    newController.comment = self.comment;
    newController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    newController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:newController animated:YES completion:nil];
}

- (IBAction)carOpenListPressed:(id)sender {
    self.popover = [ARSPopover new];
    self.popover.sourceView = self.carOpenList;
    self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.carOpenList.bounds), CGRectGetMaxY(self.carOpenList.bounds), 0, 0);
    self.popover.contentSize = CGSizeMake(200, 100);
    self.popover.arrowDirection = UIPopoverArrowDirectionUp;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            pickerView.backgroundColor = [UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
            pickerView.tintColor= [UIColor whiteColor];
            [pickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:self.selectedCarIndex inComponent:0 animated:YES];
            [popover.view addSubview:pickerView];
        }
         ];
    }];
}

#pragma mark - RPAddCommentProtocol 

- (void)addComment:(NSString *)comment{
    self.comment = comment;
    [self.addCommentButton setTitle:comment forState:UIControlStateNormal];
}

#pragma mark - UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.carsArray.count;
}


- (NSAttributedString *)pickerView:(UIPickerView *)pickerView
             attributedTitleForRow:(NSInteger)row
                      forComponent:(NSInteger)component
{
    NSString *string = [NSString stringWithFormat:@"%@(%@)",((RPCar*)self.carsArray[row]).marka,((RPCar*)self.carsArray[row]).numberCar];
    
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@
            {
            NSForegroundColorAttributeName:[UIColor whiteColor]
            }];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@(%@)",((RPCar*)self.carsArray[row]).marka,((RPCar*)self.carsArray[row]).numberCar];;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedCarIndex = row;
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"destinationAddress"]) {
        RPTabBarViewController *newController = segue.destinationViewController;
        if (self.addressTo!=nil) {
            newController.addressTo = self.addressTo;
        }
        newController.addressFrom = self.addressFrom;
        newController.addressDelegate = self;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"destinationAddress"]) {
        return YES;
    }
    if ([identifier isEqualToString:@"orderSegue"]) {
        return YES;
    }
    return NO;
}

-(void)passAddressTo:(LMAddress *)address{
    self.addressTo = address;
    NSString *text = [NSString stringWithFormat:@"%@ %@", address.locality , [address parseThoroughfare]];
    [self.sayDriverButton setTitle:text forState:UIControlStateNormal];
//    [self searchDistance];
}


#pragma mark - Actions

- (IBAction)setDateButtonPressed:(id)sender {
    
    self.popover = [ARSPopover new];
    self.popover.sourceView = self.changeDateButton;
    self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.changeDateButton.bounds), CGRectGetMaxY(self.changeDateButton.bounds), 0, 0);
    self.popover.contentSize = CGSizeMake(kScreenWidth-60, 150);
    self.popover.arrowDirection = UIPopoverArrowDirectionUp;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIDatePicker* datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
            [datePicker setLocale:locale];
            [datePicker setTimeZone:[NSTimeZone systemTimeZone]];
            [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
            if (self.selectedDate!=nil) {
                datePicker.date = self.selectedDate;
            } else datePicker.date = [[NSDate date]dateByAddingTimeInterval:60*40];
            [datePicker setMinimumDate:[[NSDate date]dateByAddingTimeInterval:60*40]];
            [datePicker setMaximumDate:[[NSDate date]dateByAddingTimeInterval:60*60*24*7]];
            [popover.view addSubview:datePicker];
        }
         ];
    }];
}

-(void)changeDate:(UIDatePicker*)datePicker{
    self.selectedDate=datePicker.date;
}


- (IBAction)callButtonPressed:(id)sender {
    NSMutableDictionary *param = [@{@"type":@"new",
                                    @"userid":@([RPUser sharedUser].userid),
                                    @"password":[RPUser sharedUser].password,
                                    @"pay_ball":@(self.balSwitch.isOn),
                                    @"user_phone":[RPUser sharedUser].phone,
                                    @"flag_reg":@(1),
                                    @"comment":self.comment,
                                    @"lan_user":@(self.latitude),
                                    @"lng_user":@(self.longitude),
                                    @"city_from":self.addressFrom.locality,
                                    @"street_from":self.addressFrom.street,
                                    @"house_from":self.addressFrom.house,
                                    @"lan_from":@(self.addressFrom.coordinate.latitude),
                                    @"lng_from":@(self.addressFrom.coordinate.longitude),
                                    }mutableCopy];
    
    if (self.addressFrom.korpus.length!=0) {
        param[@"korpus_from"]=self.addressFrom.korpus;
    }
    if (self.addressFrom.stroenie.length!=0) {
        param[@"stroenie_from"]=self.addressFrom.stroenie;
    }
    
    if (self.addressTo!=nil) {
        param[@"city_to"]=self.addressTo.locality;
        param[@"street_to"]=self.addressTo.street;
        param[@"house_to"]=self.addressTo.house;
        param[@"lan_to"]=@(self.addressTo.coordinate.latitude);
        param[@"lng_to"]=@(self.addressTo.coordinate.longitude);
        if (self.addressTo.korpus.length!=0) {
            param[@"korpus_to"]=self.addressTo.korpus;
        }
        if (self.addressTo.stroenie.length!=0) {
            param[@"stroenie_to"]=self.addressTo.stroenie;
        }
        
    }
    if (self.selectedDate==nil) {
        param[@"flag_time"]=@(1);
    } else {
        param[@"flag_time"]=@(0);
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *stringFromDate = [formatter stringFromDate:self.selectedDate];
        param[@"date_plane"]= stringFromDate;
    }
    if (self.selectedCarIndex!=-1) {
        RPCar *car = self.carsArray[self.selectedCarIndex];
        param[@"markaid"]=@(car.carId);
    }
    self.view.userInteractionEnabled = NO;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager]newOrder:param onSuccess:^(RPOrder *order, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            self.view.userInteractionEnabled = YES;;
            if (errorMessage==nil) {
                [[NSUserDefaults standardUserDefaults]setValue:[RPOrder sharedOrder].token forKey:@"token2"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"rporderviewcontroller"] animated:NO];
            } else [self showErrorMessage:@"Произошла ошибка!"];
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка!"];
        }];
    });
}
@end
