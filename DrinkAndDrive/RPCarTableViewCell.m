//
//  RPCarTableViewCell.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/4/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPCarTableViewCell.h"

@implementation RPCarTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    } else self.contentView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];    // Configure the view for the selected state
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    } else self.contentView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
}

@end
