//
//  RPSearchFullAddressViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPSearchFullAddressViewController.h"
#import "RPAddressTableViewCell.h"
#import "RPAdditionalDataViewController.h"
#import "Helper.h"

@interface RPSearchFullAddressViewController()<UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *streetTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *homeTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *housingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *buildingTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *cityTextField;
@property (nonatomic, strong) NSMutableArray <LMAddress*> *addressArray;
@end


@implementation RPSearchFullAddressViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.addressArray = [[NSMutableArray alloc]init];
    self.cityTextField.text = self.addressFrom.locality;
    self.streetTextField.text= self.addressFrom.street;
    if (self.addressFrom.korpus!=nil) {
        self.housingTextField.text = self.addressFrom.korpus;
    }
    if (self.addressFrom.house!=nil) {
        self.homeTextField.text = self.addressFrom.house;
    }
    if (self.addressFrom.stroenie!=nil) {
        self.buildingTextField.text = self.addressFrom.stroenie;
    }
}

- (IBAction)nextButtonPressed:(id)sender {
    if (self.cityTextField.text.length!=0 && self.streetTextField.text.length!=0
        && self.homeTextField.text.length!=0) {
        if (self.addressArray.count!=0) {
            [self.delegate passAddressFrom:self.addressFrom];
            [self performSegueWithIdentifier:@"additionalDataSegue" sender:self];
        }
    } else {
        if (self.cityTextField.text.length==0)
            [self.cityTextField showError];
        if (self.streetTextField.text.length==0)
            [self.streetTextField showError];
        if (self.homeTextField.text.length==0)
            [self.homeTextField showError];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
    
    if (self.streetTextField.text.length!=0 && self.cityTextField.text.length!=0) {
        [self getLocationFromAddress];
    }
}

-(void)getLocationFromAddress{
    self.addressArray = nil;
    self.addressArray = [[NSMutableArray <LMAddress*> alloc]init];
    [self.tableView reloadData];
    NSMutableString *sentence = [[NSString stringWithFormat:@"Россия,%@,%@",_cityTextField.text,_streetTextField.text]mutableCopy];
    if (self.homeTextField.text.length!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.homeTextField.text]]mutableCopy];
    }
    if (self.housingTextField.text.length!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",корпус%@",self.housingTextField.text]]mutableCopy];
    }
    if (self.buildingTextField.text.length!=0) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",строение%@",self.buildingTextField.text]]mutableCopy];
    }
    
    [[LMGeocoder sharedInstance]geocodeAddressString:sentence service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
        if (results.count && !error) {
            for (LMAddress *address in results) {
                if (![NSString isEmpty:[address parseThoroughfare]]) {
                    if ([address isCorrectAddress]) {
                        [self.addressArray addObject:address];
                    }
                }
            }
            [self.tableView reloadData];
        }
    }];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPAddressTableViewCell *cell = (RPAddressTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    LMAddress *currentAddress = self.addressArray[indexPath.row];
    cell.cityLabel.text = currentAddress.locality;
    cell.streetLabel.text = currentAddress.street;
    cell.houseLabel.text = [NSString stringWithFormat:@"дом:%@", currentAddress.house];
    if (![NSString isEmpty:currentAddress.korpus]) {
        cell.housingLabel.text = [NSString stringWithFormat:@"Корп.:%@",currentAddress.korpus];
    }
    if (![NSString isEmpty:currentAddress.stroenie]) {
        cell.buildingLabel.text = [NSString stringWithFormat:@"Стр.:%@", currentAddress.stroenie];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LMAddress *currentAddress = self.addressArray[indexPath.row];
    [self.delegate passAddressFrom:currentAddress];
    [self performSegueWithIdentifier:@"additionalDataSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (self.addressArray.count!=0) {
        if ([segue.identifier isEqualToString:@"additionalDataSegue"]) {
            RPAdditionalDataViewController *newController = [segue destinationViewController];
            if (indexPath==nil) {
                newController.addressFrom = self.addressArray[0];
            } else newController.addressFrom = self.addressArray[indexPath.row];
            newController.latitude = self.latitude;
            newController.longitude = self.longitude;
        }
    }
}
@end
