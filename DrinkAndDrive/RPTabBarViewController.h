//
//  RPTabBarViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface RPTabBarViewController : UITabBarController
@property (nonatomic, weak) id<RPChangeAddressTo> addressDelegate;
@property (nonatomic, strong) LMAddress *addressFrom;
@property (nonatomic, strong) LMAddress *addressTo;
- (IBAction)menuButtonTapped:(id)sender;
@end
