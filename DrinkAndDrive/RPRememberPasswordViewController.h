//
//  RPRememberPasswordViewController.h
//  DrinkNDrive
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRememberPasswordViewController : UIViewController

@property (nonatomic, assign) BOOL hideAdditionalText;

@end
