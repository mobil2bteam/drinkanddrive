//
//  ViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "ViewController.h"
#import "RPAboutInWebViewController.h"
#import "RPOrderViewController.h"
#import "AppDelegate.h"
#import "RPRememberPasswordViewController.h"

#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kAnimationDuration 0.35
@interface ViewController ()<UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate, ARSPopoverDelegate>
{
    __weak IBOutlet UIScrollView *scrollView;
}
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFieldsArray;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField4;
@property (weak, nonatomic) IBOutlet UITextField *phoneUserTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneUserTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneUserTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneUserTextField4;
@property (nonatomic, strong) NSString *phoneUser;
@property (nonatomic, strong) NSString *phone;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *passwordTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *nameUserTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *passwordUserTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *carModelTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *carNumberTextField;
@property (weak, nonatomic) IBOutlet UIImageView *carModelImageView;
@property (weak, nonatomic) IBOutlet UIImageView *dayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *monthImageView;
@property (weak, nonatomic) IBOutlet UIView *authorizeView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *stepLabel;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UISwitch *finalSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *authorizeAlign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *welcomeAlign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewAlign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carViewAlign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confidenceAlign;
@property (assign,nonatomic) NSInteger selectedCarTypeIndex;
@property (assign,nonatomic) NSInteger selectedMonthIndex;
@property (assign,nonatomic) NSInteger selectedDayIndex;
@property (assign,nonatomic) NSInteger step;
@property (nonatomic, strong) NSArray *monthArray;
@property (nonatomic, strong) NSArray *dayArray;
@property (strong) ARSPopover *popover;
@property (nonatomic, strong) NSUserDefaults *userDefaults;
@property (nonatomic) BOOL isLoaded;
@property (nonatomic) AppDelegate* appDelegate;
@end

@implementation ViewController

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    self.selectedCarTypeIndex=self.selectedMonthIndex=self.selectedDayIndex=self.step=0;
    self.welcomeAlign.constant = self.carViewAlign.constant = self.confidenceAlign.constant = self.bottomViewAlign.constant = kScreenWidth;
    UISwipeGestureRecognizer * swipeRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!self.isLoaded) {
        [self loadData];
    }
}

-(void)loadData{
    self.isLoaded = YES;
    if (![self isReachility]) {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
        alert.backgroundType = Blur;
        alert.shouldDismissOnTapOutside = NO;
        [alert addButton:@"Повторить" target:self selector:@selector(loadData)];
        [alert showError:self title:nil
                subTitle:@"Отсутствует соединение с интернетом, проверьте, пожалуйста, подключение!"
        closeButtonTitle:nil duration:0.0f];
    } else [self load];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.userDefaults stringForKey:@"phone"]!=nil) {
        NSString *phone = [self.userDefaults stringForKey:@"phone"];
        self.phoneTextField1.text = [phone substringWithRange:NSMakeRange(0, 3)];
        self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
        self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
        self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Properties
-(NSArray*)monthArray{
    if (_monthArray) return _monthArray;
    return @[@"Январь",@"Февраль",@"Март",@"Апрель",@"Май",@"Июнь",
             @"Июль",@"Август",@"Сентябрь",@"Октябрь",@"Ноябрь",@"Декабрь"
             ];
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

- (NSString*)phoneUser{
    NSString *phoneUser = [NSString stringWithFormat:@"%@%@%@%@", self.phoneUserTextField1.text, self.phoneUserTextField2.text, self.phoneUserTextField3.text, self.phoneUserTextField4.text];
    return phoneUser;
}

-(NSArray*)dayArray{
    NSInteger index;
    if (self.selectedMonthIndex==0 | self.selectedMonthIndex==2 |self.selectedMonthIndex==4 |
        self.selectedMonthIndex==6 | self.selectedMonthIndex==7 |self.selectedMonthIndex==9 |
        self.selectedMonthIndex==11) index = 31;
    else if (self.selectedMonthIndex==3 | self.selectedMonthIndex==5 |
             self.selectedMonthIndex==8 | self.selectedMonthIndex==10) index = 30;
    else index = 29;
    NSMutableArray *days = [[NSMutableArray alloc]init];
    for (NSInteger i=1; i<=index; i++) {
        [days addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    return [NSArray arrayWithArray:days];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    if (textField == self.nameUserTextField && self.phoneUser.length != 10) {
        [self.phoneUserTextField1 becomeFirstResponder];
    }
    if (textField == self.carModelTextField) {
        [self.carNumberTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField == self.passwordTextField ||
        textField == self.passwordUserTextField ||
        textField == self.nameUserTextField ||
        textField == self.carModelTextField ||
        textField == self.carNumberTextField) {
        if (textField.text.length==0) {
            [textField showError];
        }else{
            [textField hideError];
        }
    }
}


-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4 ||
        textField == self.phoneUserTextField1 ||
        textField == self.phoneUserTextField2 ||
        textField == self.phoneUserTextField3 ||
        textField == self.phoneUserTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneUserTextField4) {
                    [self.phoneUserTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneUserTextField3) {
                    [self.phoneUserTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneUserTextField2) {
                    [self.phoneUserTextField1 becomeFirstResponder];
                }
                
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4 ||
        textField == self.phoneUserTextField3 ||
        textField == self.phoneUserTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            if (textField == self.phoneUserTextField3) {
                [self.phoneUserTextField4 becomeFirstResponder];
            }
            if (textField == self.phoneTextField4) {
                [self.passwordTextField becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneUserTextField1 ||
        textField == self.phoneUserTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneUserTextField1) {
                    [self.phoneUserTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneUserTextField2) {
                    [self.phoneUserTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}


#pragma mark - Actions

-(IBAction)backToTheStart:(UIStoryboardSegue*)segue{
    self.backgroundView.hidden=YES;
    self.passwordTextField.text=@"";
    [self.passwordTextField hideError];
    self.carModelTextField.text=@"";
    self.carNumberTextField.text=@"";
    self.passwordUserTextField.text=@"";
    self.nameUserTextField.text=@"";
    self.authorizeAlign.constant = 0;
    [self.nextButton setTitle:@"ДАЛЕЕ" forState:UIControlStateNormal];
    self.selectedCarTypeIndex=self.selectedMonthIndex=self.selectedDayIndex=self.step=0;
    self.welcomeAlign.constant = self.carViewAlign.constant = self.confidenceAlign.constant = self.bottomViewAlign.constant = kScreenWidth;
    [self.view layoutSubviews];
}


-(IBAction)showAgreement:(id)sender{
    UINavigationController *navControler = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutInWebNavController"];
    RPAboutInWebViewController *aboutController = navControler.viewControllers[0];
    aboutController.urlAddress =  [RPOptions sharedOptions].user_agreement;
    aboutController.needHiddenMenuButton=YES;
    [self presentViewController:navControler animated:YES completion:nil];
}


- (IBAction)openListButtonPressed:(id)sender {
    [self.view endEditing:YES];
    switch (self.step) {
        case 1:
        {
            if (self.nameUserTextField.text.length==0 || self.phoneUser.length != 10) {
                if (self.nameUserTextField.text.length==0) {
                    [self.nameUserTextField showError];
                }
                if (self.phoneUser.length != 10) {
                    [self showErrorMessage:@"Неправильный телефон"];
                }
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                return;
            }
            self.step=2;
            self.stepLabel.text=@"2/3";
            self.welcomeAlign.constant = -kScreenWidth;
            self.carViewAlign.constant=0;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                [self.view layoutIfNeeded];
            }];
        }
            break;
        case 2:
        {
            if (self.carModelTextField.text.length==0 || self.carNumberTextField.text.length==0) {
                if (self.carModelTextField.text.length==0) {
                    [self.carModelTextField showError];
                }
                if (self.carNumberTextField.text.length==0) {
                    [self.carNumberTextField showError];
                }
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                return;
            }
            self.step=3;
            self.stepLabel.text=@"3/3";
            [self.nextButton setTitle:@"РЕГИСТРАЦИЯ" forState:UIControlStateNormal];
            self.carViewAlign.constant = -kScreenWidth;
            self.confidenceAlign.constant=0;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                [self.view layoutIfNeeded];
            }];
        }
            break;
            
        case 3:
        {
            if (self.passwordUserTextField.text.length==0 || !self.finalSwitch.isOn) {
                if (self.passwordUserTextField.text.length==0) {
                    [self.passwordUserTextField showError];
                }
                if (!self.finalSwitch.isOn) {
                    [self showWarningMessage:@"Примите, пожалуйста, пользовательское соглашение!"];
                }
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                return;
            }
            [self registration];
        }
            break;
    }
}

- (IBAction)registerButtonPressed:(id)sender {
    [self.view endEditing:YES];
    self.authorizeAlign.constant = -kScreenWidth;
    self.welcomeAlign.constant = self.bottomViewAlign.constant = 0;
    self.step++;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)loginButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.passwordTextField.text.length==0 || self.phone.length != 10){
        if (self.passwordTextField.text.length==0) {
            [self.passwordTextField showError];
        }
        if (self.phone.length != 10) {
            [self showErrorMessage:@"Неправильный телефон"];
        }
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    
    NSDictionary *dic = @{@"type":@"login",
                          @"phone": self.phone,
                          @"password":[self.passwordTextField.text md5]
                          };
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getRootCategoriesWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
            if (errorMessage==nil) {
                [self.appDelegate sendTokenToServer];
                [self checkExistTokenFromServer];
            }
            else {
                [self hideIndicatorView];
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
    
}

- (IBAction)dayButtonPressed:(UIButton*)button {
    self.popover = [ARSPopover new];
    NSInteger selectedIndex;
    if (button.tag==0) {
        self.popover.sourceView = self.carModelImageView;
        self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.carModelImageView.bounds), CGRectGetMinY(self.carModelImageView.bounds), 0, 0);
        self.popover.contentSize = CGSizeMake(150, 100);
        selectedIndex = self.selectedCarTypeIndex;
    } else if (button.tag==1){
        self.popover.sourceView = self.monthImageView;
        self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.monthImageView.bounds), CGRectGetMinY(self.monthImageView.bounds), 0, 0);
        self.popover.contentSize = CGSizeMake(130, 150);
        selectedIndex = self.selectedMonthIndex;
        
    } else if(button.tag==2){
        self.popover.sourceView = self.dayImageView;
        self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.dayImageView.bounds), CGRectGetMinY(self.dayImageView.bounds), 0, 0);
        self.popover.contentSize = CGSizeMake(60, 150);
        selectedIndex = self.selectedDayIndex;
    }
    self.popover.arrowDirection = UIPopoverArrowDirectionDown;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    
    
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            pickerView.backgroundColor = [UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
            pickerView.tintColor= [UIColor whiteColor];
            [pickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:selectedIndex inComponent:0 animated:YES];
            [popover.view addSubview:pickerView];
        }
         ];
    }];
    
}

- (IBAction)forgotPasswordButtonPressed:(id)sender {
    RPRememberPasswordViewController *vc = [[RPRememberPasswordViewController alloc] initWithNibName:@"RPRememberPasswordViewController" bundle:nil];
    vc.hideAdditionalText = YES;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Methods

-(void)hideIndicatorView{
    self.backgroundView.hidden=YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
    switch (self.step--) {
        case 1:
        {
            self.authorizeAlign.constant = 0;
            self.welcomeAlign.constant = self.bottomViewAlign.constant = kScreenWidth;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                [self.view layoutIfNeeded];
            }];
        }
            break;
        case 2:
        {
            self.stepLabel.text=@"1/3";
            self.welcomeAlign.constant = 0;
            self.carViewAlign.constant=kScreenWidth;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                [self.view layoutIfNeeded];
                
            }];
        }
            break;
            
        case 3:
        {
            [self.nextButton setTitle:@"ДАЛЕЕ" forState:UIControlStateNormal];
            self.stepLabel.text=@"2/3";
            self.carViewAlign.constant = 0;
            self.confidenceAlign.constant=kScreenWidth;
            [UIView animateWithDuration:kAnimationDuration animations:^{
                [self.view layoutIfNeeded];
            }];
        }
            break;
    }
}

-(void)load{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // подгружаем основные настройки для приложения
        [[RPServerManager sharedManager] getConfigsOnSuccess:^(RPOptions *configs) {
            // выполняем вход, если нужно
            [self checkQuickOrderStatus];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
            alert.backgroundType = Blur;
            alert.shouldDismissOnTapOutside = NO;
            [alert addButton:@"Повторить" target:self selector:@selector(loadData)];
            [alert showError:self title:nil
                    subTitle:@"Произошла ошибка"
            closeButtonTitle:nil duration:0.0f];
        }];
    });
}

-(void)registration{
    NSMutableDictionary *dic = [@{@"type":@"new",
                                  @"name":self.nameUserTextField.text,
                                  @"phone": self.phoneUser,
                                  @"password":[self.passwordUserTextField.text md5],
                                  @"marka":self.carModelTextField.text,
                                  @"number":self.carNumberTextField.text,
                                  @"typecar":@(self.selectedCarTypeIndex+1),
                                  @"happyday":@(self.selectedDayIndex+1),
                                  @"happymonth":@(self.selectedMonthIndex+1)
                                  }mutableCopy];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getRootCategoriesWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self.appDelegate sendTokenToServer];
                [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NavigationMapViewController"]  animated:NO completion:nil];
            }
            else [self showErrorMessage:errorMessage];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}


#pragma mark - UIPickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) return self.monthArray.count;
    else if ([self.popover.sourceView isEqual:self.dayImageView]) return self.dayArray.count;
    return [RPOptions sharedOptions].typecar.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView
             attributedTitleForRow:(NSInteger)row
                      forComponent:(NSInteger)component
{
    NSString *string;
    if ([self.popover.sourceView isEqual:self.monthImageView]) string =  self.monthArray[row];
    else if ([self.popover.sourceView isEqual:self.dayImageView])
        string =  self.dayArray[row];
    else  string = [[RPOptions sharedOptions].typecar[row] valueForKey:@"name"];
    
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@
            {
            NSForegroundColorAttributeName:[UIColor whiteColor]
            }];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        return self.monthArray[row];
    } else if ([self.popover.sourceView isEqual:self.dayImageView])
        return self.dayArray[row];
    return [[RPOptions sharedOptions].typecar[row] valueForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) self.selectedMonthIndex=row;
    else if ([self.popover.sourceView isEqual:self.dayImageView]) self.selectedDayIndex=row;
    else self.selectedCarTypeIndex = row;
}

#pragma mark  - <ARSPopoverDelegate>
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        self.monthLabel.text = self.monthArray[self.selectedMonthIndex];
        self.selectedDayIndex=0;
        self.dayLabel.text = self.dayArray[self.selectedDayIndex];
    } else if ([self.popover.sourceView isEqual:self.dayImageView]) {
        self.dayLabel.text = self.dayArray[self.selectedDayIndex];
    } else
        [self.carTypeLabel setText:[[RPOptions sharedOptions].typecar[self.selectedCarTypeIndex] valueForKey:@"name"]];
}

#pragma mark - Keyboard Notifications
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    [UIView animateWithDuration:0.26 animations:^{
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.26 animations:^{
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }];
}




#pragma mark - Check order status

-(void)checkQuickOrderStatus{
    if ([self.userDefaults valueForKey:@"token"]!=nil) {
        RPOrderViewController *newController = [self.storyboard instantiateViewControllerWithIdentifier:@"rporderviewcontroller"];
        NSDictionary *param = @{@"type":@"get",
                                @"token":[self.userDefaults valueForKey:@"token"]};
        [[RPServerManager sharedManager]getOrder:param onSuccess:^(RPOrder *order, NSString *errorMessage) {
            if (errorMessage==nil) {
                [self presentViewController:newController animated:NO completion:^{
                    [self hideIndicatorView];
                }];
            } else {
                [self hideIndicatorView];
                [self.userDefaults removeObjectForKey:@"token"];
                [self.userDefaults synchronize];
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
            
        }];
    } else
        if ([self.userDefaults valueForKey:@"phone"] && [self.userDefaults valueForKey:@"password"])
        { //если пользователь авторизован, то проверяем его статус (статус пользователя, не заблокирован ли он
            NSDictionary *dic = @{@"type":@"login",
                                  @"phone":[self.userDefaults valueForKey:@"phone"],
                                  @"password":[self.userDefaults valueForKey:@"password"]                                  };
            [[RPServerManager sharedManager] getRootCategoriesWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
                if (errorMessage==nil) {
                    [self.appDelegate sendTokenToServer];
                    [self checkExistTokenFromServer];
                }
                else {
                    [self.userDefaults removeObjectForKey:@"phone"];
                    [self.userDefaults removeObjectForKey:@"password"];
                    [self.userDefaults synchronize];
                    [self hideIndicatorView];
                    [self showErrorMessage:errorMessage];
                }
                
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [self hideIndicatorView];
                [self showErrorMessage:@"Произошла ошибка"];
            }];
        } else {
            // если он не авторизован
            [self hideIndicatorView];
        }
}


-(void)showFullOrderStatusController{
    ECSlidingViewController *newController = [self.storyboard instantiateViewControllerWithIdentifier:@"NavigationMapViewController"];
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"METransitionsNavigationController"];
    MapViewController *mapContoller = navController.viewControllers[0];
    newController.topViewController = navController;
    NSDictionary *param = @{@"type":@"get",
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            @"token":[self.userDefaults valueForKey:@"token2"]};
    [[RPServerManager sharedManager]getOrder:param onSuccess:^(RPOrder *order, NSString *errorMessage) {
        if (errorMessage==nil) {
            [mapContoller pushOrderViewController:order];
            [self presentViewController:newController animated:NO completion:^{
                [self hideIndicatorView];
            }];
        } else {
            [self hideIndicatorView];
            [self showErrorMessage:errorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideIndicatorView];
        [self showErrorMessage:@"Произошла ошибка"];
    }];
}

-(void)checkExistTokenFromServer{
    NSDictionary *param = @{@"type":@"get",
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            };
    [[RPServerManager sharedManager]checkOrder:param onSuccess:^(NSString *token, NSString *errorMessage) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (errorMessage==nil) {
            if (token!=nil && !([RPOrder sharedOrder].status>6 && [RPOrder sharedOrder].status<10)) {
                // если получили токен от сервера, то записываем его и показываем состояние заказа
                [self.userDefaults setObject:token forKey:@"token2"];
                [self.userDefaults synchronize];
                [self showFullOrderStatusController];
            } else {
                [self.userDefaults removeObjectForKey:@"token2"];
                [self.userDefaults synchronize];
                
                [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NavigationMapViewController"]  animated:NO completion:^{
                    [self hideIndicatorView];
                }];
            }
        } else {
            [self hideIndicatorView];
            [self showErrorMessage:errorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideIndicatorView];
        [self showErrorMessage:@"Произошла ошибка"];
        
    }];
}
@end
