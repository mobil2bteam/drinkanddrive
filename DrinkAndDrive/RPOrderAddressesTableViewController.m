//
//  RPOrderAddressesTableViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrderAddressesTableViewController.h"
#import "RPOrderAddressesTableViewCell.h"
#import "RPServerManager.h"
#import "RPUser.h"
#import "RPOrderDescriptionTableViewController.h"
#import "RPOrderTabBarController.h"
@interface RPOrderAddressesTableViewController()
@property (nonatomic, strong) NSArray *wayList;
@end
@implementation RPOrderAddressesTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    RPOrderDescriptionTableViewController *descriptionOrderVeiwController =  (RPOrderDescriptionTableViewController*) self.tabBarController.viewControllers[0];
    if (descriptionOrderVeiwController.orderFull.wayList.count!=0) {
        self.wayList = descriptionOrderVeiwController.orderFull.wayList;
        [self.tableView reloadData];
    }
}
#pragma mark - UITableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.wayList.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPOrderAddressesTableViewCell *cell = (RPOrderAddressesTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    RPLocalAddress *addressFrom = self.wayList[indexPath.row][0];
    RPLocalAddress *addressTo = self.wayList[indexPath.row][1];
    cell.addressALabel.text = [NSString stringWithFormat:@"%@ %@ %@", addressFrom.city, addressFrom.street, addressFrom.house];
    cell.addressBLabel.text = [NSString stringWithFormat:@"%@ %@ %@", addressTo.city, addressTo.street, addressTo.house];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}



@end
