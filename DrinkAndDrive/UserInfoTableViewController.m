//
//  UserInfoTableViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "UserInfoTableViewController.h"
#import "RPInviteViewController.h"
#import "Helper.h"
#import "RPOptions.h"

@interface UserInfoTableViewController ()<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, ARSPopoverDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *balLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *monthImageView;
@property (nonatomic, strong) NSArray *monthArray;
@property (nonatomic, strong) NSArray *dayArray;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField4;
@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UIView *progressBar;
@property (weak, nonatomic) IBOutlet UIView *innerProgressBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerProgressBarWidthConstraint;

@property (strong) ARSPopover *popover;
@property (assign) NSInteger selectedMonthIndex;
@property (assign) NSInteger selectedDayIndex;

@end

@implementation UserInfoTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.balLabel.text = [NSString stringWithFormat:@"%ld",(long)[RPUser sharedUser].ball];
    self.monthArray = @[@"Январь",
                        @"Февраль",
                        @"Март",
                        @"Апрель",
                        @"Май",
                        @"Июнь",
                        @"Июль",
                        @"Август",
                        @"Сентябрь",
                        @"Октябрь",
                        @"Ноябрь",
                        @"Декабрь"
                        ];
    self.tableView.backgroundColor = [UIColor colorWithRed:0.15 green:0.16 blue:0.19 alpha:1.0];
    self.nameTextField.text = [RPUser sharedUser].name;
    NSString *phone = [RPUser sharedUser].phone;
    self.phoneTextField1.text = [phone substringWithRange:NSMakeRange(0, 3)];
    self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
    self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
    self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
    if ([RPUser sharedUser].happyday!=0) {
        self.selectedDayIndex = [RPUser sharedUser].happyday-1;
    } else self.selectedDayIndex=0;
    if ([RPUser sharedUser].happymonth!=0) {
        self.selectedMonthIndex = [RPUser sharedUser].happymonth-1;
    } else self.selectedMonthIndex=0;
    [self generateArrayWithDays];
    self.dayLabel.text = self.dayArray[self.selectedDayIndex];
    self.monthLabel.text = self.monthArray[self.selectedMonthIndex];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    self.progressBar.layer.masksToBounds = YES;
    self.progressBar.layer.borderColor = [UIColor whiteColor].CGColor;
    self.progressBar.layer.borderWidth = 1.f;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //запускаем анимацию шкалы баллов
    CGFloat progressBarWidth = CGRectGetWidth(self.progressBar.bounds);
    NSInteger maxBall = [RPOptions sharedOptions].max_ball;
    NSInteger countBall = [RPUser sharedUser].ball;
    CGFloat ballInPercent = (countBall * 1.0)/maxBall;
    [UIView animateWithDuration:0.5 animations:^{
        self.innerProgressBarWidthConstraint.constant = progressBarWidth * ballInPercent;
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}


- (IBAction)monthButtonPressed:(id)sender {
    [self.view endEditing:YES];
    self.popover = [ARSPopover new];
    self.popover.sourceView = self.monthImageView;
    self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.monthImageView.bounds), CGRectGetMaxY(self.monthImageView.bounds), 0, 0);
    self.popover.contentSize = CGSizeMake(130, 150);
    self.popover.arrowDirection = UIPopoverArrowDirectionUp;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    
    
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            pickerView.backgroundColor = [UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
            pickerView.tintColor= [UIColor whiteColor];
            [pickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:self.selectedMonthIndex inComponent:0 animated:YES];
            [popover.view addSubview:pickerView];
        }
         ];
    }];
    
}
- (IBAction)dayButtonPressed:(id)sender {
    [self.view endEditing:YES];
    self.popover = [ARSPopover new];
    self.popover.sourceView = self.dayImageView;
    self.popover.sourceRect = CGRectMake(CGRectGetMidX(self.dayImageView.bounds), CGRectGetMaxY(self.dayImageView.bounds), 0, 0);
    self.popover.contentSize = CGSizeMake(60, 150);
    self.popover.arrowDirection = UIPopoverArrowDirectionUp;
    self.popover.delegate = self;
    self.popover.backgroundColor =[UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
    
    
    [self presentViewController:self.popover animated:YES completion:^{
        [self.popover insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight) {
            UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake (0, 0, self.popover.contentSize.width, self.popover.contentSize.height)];
            pickerView.backgroundColor = [UIColor colorWithRed:0.1093 green:0.1229 blue:0.1443 alpha:1.0];
            pickerView.tintColor= [UIColor whiteColor];
            [pickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:self.selectedDayIndex inComponent:0 animated:YES];
            [popover.view addSubview:pickerView];
            
        }
         ];
    }];
    
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

-(void)generateArrayWithDays{
    
    NSInteger index;
    if (self.selectedMonthIndex==0 |
        self.selectedMonthIndex==2 |
        self.selectedMonthIndex==4 |
        self.selectedMonthIndex==6 |
        self.selectedMonthIndex==7 |
        self.selectedMonthIndex==9 |
        self.selectedMonthIndex==11)
    {
        index = 31;
    } else if (self.selectedMonthIndex==3 |
               self.selectedMonthIndex==5 |
               self.selectedMonthIndex==8 |
               self.selectedMonthIndex==10)
    {
        index = 30;
    }
    else {
        index = 29;
    }

    
    NSMutableArray *days = [[NSMutableArray alloc]init];
    for (NSInteger i=1; i<=index; i++) {
        [days addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    self.dayArray =  [NSArray arrayWithArray:days];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        return self.monthArray.count;
    } else {
        [self generateArrayWithDays];
        return self.dayArray.count;
    }
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView
             attributedTitleForRow:(NSInteger)row
                      forComponent:(NSInteger)component
{
    NSString *string;
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        string =  self.monthArray[row];
    } else string =  self.dayArray[row];
    
    
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@
            {
            NSForegroundColorAttributeName:[UIColor whiteColor]
            }];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        return self.monthArray[row];
    } else return self.dayArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        self.selectedMonthIndex=row;
    } else {
        self.selectedDayIndex=row;
    }
}

#pragma mark  - <ARSPopoverDelegate>
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    if ([self.popover.sourceView isEqual:self.monthImageView]) {
        self.monthLabel.text = self.monthArray[self.selectedMonthIndex];
        self.selectedDayIndex=0;
        self.dayLabel.text = self.dayArray[self.selectedDayIndex];
    } else  {
        self.dayLabel.text = self.dayArray[self.selectedDayIndex];
    }
}



-(void)saveUser:(RPUser*)user{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"user"];
    [userDefaults synchronize];
    [userDefaults setObject: [user userToDictionary] forKey:@"user"];
    [userDefaults synchronize];
}


- (IBAction)saveButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.phone.length != 10){
        [self showErrorMessage:@"Неправильный телефон"];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    
    NSMutableDictionary *dic = [@{@"type":@"edit",
                                  @"name":self.nameTextField.text,
                                  @"phone": self.phone,
                                  @"password":[RPUser sharedUser].password,
                                  @"id":@([RPUser sharedUser].userid),
                                  @"happyday":@(self.selectedDayIndex+1),
                                  @"happymonth":@(self.selectedMonthIndex+1)
                                  }mutableCopy];
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getRootCategoriesWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
                alert.backgroundType = Blur;
                alert.shouldDismissOnTapOutside = YES;
                [alert showSuccess:self title:nil subTitle:@"Ваши данные успешно изменены!" closeButtonTitle:@"Ok" duration:0.0f];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка!"];
        }];
    });
}
- (IBAction)exitButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
    [[NSUserDefaults standardUserDefaults ]removeObjectForKey:@"token2"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"getBal"]) {
        RPInviteViewController *newController = [segue destinationViewController];
        newController.needHiddenMenuButton = YES;
    }
}

#pragma mark - UITableView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

@end
