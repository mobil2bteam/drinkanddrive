//
//  RPTabBarViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPTabBarViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface RPTabBarViewController ()
@end

@implementation RPTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.547 green:1.0 blue:0.3281 alpha:1.0]];
    // Do any additional setup after loading the view.
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}
@end
