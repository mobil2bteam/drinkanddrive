//
//  RPMenuTableViewCell.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPMenuTableViewCell.h"

@implementation RPMenuTableViewCell

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted)
        self.menuLabel.textColor = [UIColor colorWithRed:0.98 green:0.69 blue:0.18 alpha:1.0];
    else self.menuLabel.textColor = [UIColor whiteColor];
}
@end
