//
//  RPCancelOrderWithRefuseAlertController.h
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RPCancelOrderWithRefuseProtocol <NSObject>

- (void)cancelOrderWithRefuseID:(NSInteger)refuseID;

@end

@interface RPCancelOrderWithRefuseAlertController : UIViewController

@property (nonatomic, strong) id <RPCancelOrderWithRefuseProtocol> delegate;

@end
