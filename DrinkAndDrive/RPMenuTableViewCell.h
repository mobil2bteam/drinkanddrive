//
//  RPMenuTableViewCell.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@end
