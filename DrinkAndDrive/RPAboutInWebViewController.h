//
//  RPAboutInWebViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPAboutInWebViewController : UIViewController
- (IBAction)menuButtonTapped:(id)sender;
@property (nonatomic, strong) NSString *urlAddress;
@property (nonatomic, assign) BOOL needHiddenMenuButton;;
@end
