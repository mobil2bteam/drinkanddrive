//
//  RPAboutInWebViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPAboutInWebViewController.h"
#import "Helper.h"
@interface RPAboutInWebViewController()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation RPAboutInWebViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
    [self addLogoToNavigationBar];
    if (self.needHiddenMenuButton) {
        [self addLeftBarButton];
    }
    NSURL *websiteUrl = [NSURL URLWithString:self.urlAddress];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:websiteUrl];
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [self.webView loadRequest:urlRequest];
    self.navigationController.navigationBar.hidden=NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.needHiddenMenuButton) {
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}
@end
