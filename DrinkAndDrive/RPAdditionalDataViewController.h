//
//  RPAdditionalDataViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 1/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "Helper.h"

@protocol RPAddCommentProtocol <NSObject>

- (void)addComment:(NSString *)comment;

@end

@interface RPAdditionalDataViewController : UIViewController<RPChangeAddressTo, RPAddCommentProtocol>
@property(nonatomic, assign) CLLocationDegrees latitude;
@property(nonatomic, assign) CLLocationDegrees longitude;
@property (nonatomic, strong) LMAddress *addressFrom;
@end
