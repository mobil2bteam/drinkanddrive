//
//  RPOrderDescriptionTableViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/24/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrderDescriptionTableViewController.h"
#import "Helper.h"
@interface RPOrderDescriptionTableViewController()
@property (weak, nonatomic) IBOutlet UILabel *numberOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDateCreateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDateFinishLabel;
@property (weak, nonatomic) IBOutlet UILabel *summLabel;
@property (weak, nonatomic) IBOutlet UILabel *summDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *ballLabel;
@property (weak, nonatomic) IBOutlet UILabel *itogLabel;
@property (weak, nonatomic) IBOutlet UILabel *markaLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverSurnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carHeadLabel;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageVeiw;

@property (weak, nonatomic) IBOutlet UITableViewCell *ocenkaCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *driverCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *driverDescriptionCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *carTopCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *carBottomCell;

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *labels;

//ячейка с комментарием
@property (weak, nonatomic) IBOutlet UITableViewCell *commentTopCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *commentBottomCell;
@property (weak, nonatomic) IBOutlet UILabel *commentRootLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentDescriptionLabel;


//ячейка с оценками
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *driverRatingImageView;
@property (weak, nonatomic) IBOutlet UIButton *callDriverImageView;


//ячейка с оценками
@property (weak, nonatomic) IBOutlet UILabel *ocenkaLabel;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *ocenkaImageView;

@property (nonatomic, assign) BOOL canDelete;
@property (nonatomic, assign) BOOL canDeleteOcenkaCell;

@end
@implementation RPOrderDescriptionTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
    self.tableView.scrollEnabled = NO;
    [self hideAllControlsFromTableView:YES];
    NSDictionary *param = @{@"type":@"get",
                            @"full_info":@(1),
                            @"userid":@([RPUser sharedUser].userid),
                            @"password":[RPUser sharedUser].password,
                            @"token":((RPOrderTabBarController*)self.tabBarController).token
                            };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getFullInfo:param onSuccess:^(RPOrderFull *orderFull, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage==nil) {
                self.orderFull = orderFull;
                self.numberOrderLabel.text = [NSString stringWithFormat:@"%ld", (long)orderFull.numberOrder];
                self.commentDescriptionLabel.text = orderFull.comment;
                self.orderDateFinishLabel.text= orderFull.dateFinish;
                self.orderDateCreateLabel.text=orderFull.dateCreate;
                self.summLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)orderFull.summ];
                self.ballLabel.text = [NSString stringWithFormat:@"%ld", (long)orderFull.addBall];
                self.summDiscountLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)orderFull.summBall];
                self.itogLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)orderFull.itog];
                self.markaLabel.text = orderFull.marka;
                self.carNumberLabel.text = orderFull.numberCar;
                self.commentDescriptionLabel.text = self.orderFull.comment;
                [self fillDriverInfoView];
                [self hideAllControlsFromTableView:NO];
                if (orderFull.ocenka!=0)
                    for (NSInteger i=0; i<self.orderFull.ocenka; i++) {
                        UIImageView *starImageVeiw = self.ocenkaImageView[i];
                        starImageVeiw.image = [UIImage imageNamed:@"Star"];
                    }
                self.tableView.scrollEnabled=YES;
                self.canDelete=YES;
                [self.tableView reloadData];
            } else [self showErrorMessage:@"Произошла ошибка"];
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    });
}

-(void)hideAllControlsFromTableView:(BOOL)hide{
    self.driverImageVeiw.hidden = hide;
    for (UILabel* label in self.labels) {
        label.hidden = hide;
    }
    for (UIImageView* imageVeiw in self.driverRatingImageView) {
        imageVeiw.hidden = hide;
    }
    for (UIImageView* imageVeiw in self.ocenkaImageView) {
        imageVeiw.hidden = hide;
    }
}
-(void)fillDriverInfoView{
    self.driverImageVeiw.image = nil;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[RPOptions sharedOptions].url_suite,self.orderFull.driver.foto]];
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.driverImageVeiw setImageWithURLRequest:imageRequest
                                placeholderImage:nil                                          success:nil
                                         failure:nil];
    NSString *surname =[self.orderFull.driver.name componentsSeparatedByString:@" "][0];
    self.driverSurnameLabel.text =surname;
    self.driverNameLabel.text = [self.orderFull.driver.name substringFromIndex:surname.length+1];
        self.callDriverImageView.hidden=self.orderFull.driver.phone.length==0;
    for (NSInteger i=0; i<self.orderFull.driver.reting; i++) {
        UIImageView *starImageVeiw = self.driverRatingImageView[i];
        starImageVeiw.image = [UIImage imageNamed:@"Star"];
    }
}

#pragma mark - UITableView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if ([cell isEqual: self.ocenkaCell] && self.orderFull.ocenka==0 && self.canDelete) {
        for (UIImageView *imageView in self.ocenkaImageView) {
            [imageView removeFromSuperview];
        }
        [self.ocenkaLabel removeFromSuperview];
        return 0;
    }
    if (self.orderFull.marka.length==0 && self.canDelete) {
        if (cell == self.carTopCell) {
            [self.carHeadLabel removeFromSuperview];
            return 0;
        }
        if (cell == self.carBottomCell) {
            [self.markaLabel removeFromSuperview];
            [self.carNumberLabel removeFromSuperview];
            return 0;
        }
    }
    if (self.orderFull.comment.length == 0 && self.canDelete) {
        if (cell == self.commentTopCell) {
            [self.commentRootLabel removeFromSuperview];
            return 0;
        }
        if (cell == self.commentBottomCell) {
            [self.commentDescriptionLabel removeFromSuperview];
            return 0;
        }
    }
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (IBAction)callDriver:(id)sender {
    if (self.orderFull.driver.phone.length!=0) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",self.orderFull.driver.phone]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}
@end
