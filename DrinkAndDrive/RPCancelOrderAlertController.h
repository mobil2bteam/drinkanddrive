//
//  RPCancelOrderAlertController.h
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RPCancelOrderProtocol <NSObject>

- (void)cancelOrder;

@end

@interface RPCancelOrderAlertController : UIViewController

@property (nonatomic, strong) id <RPCancelOrderProtocol> delegate;

@end
