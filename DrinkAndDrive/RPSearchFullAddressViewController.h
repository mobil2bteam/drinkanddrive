//
//  RPSearchFullAddressViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/9/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPLocalAddress.h"
#import "LMGeocoder/LMAddress.h"
@protocol RPSearchFullAddressViewControllerDelegate;

@interface RPSearchFullAddressViewController : UIViewController
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *house;
@property (nonatomic, strong) NSString *korpus;
@property (nonatomic, strong) NSString *stroenie;
@property (nonatomic, strong) LMAddress *addressFrom;
@property (nonatomic, weak) id<RPSearchFullAddressViewControllerDelegate> delegate;
@property(nonatomic, assign) CLLocationDegrees latitude;
@property(nonatomic, assign) CLLocationDegrees longitude;
@end

@protocol RPSearchFullAddressViewControllerDelegate <NSObject>
@required
- (void)passAddressFrom:(LMAddress *)addressFrom;
@end
