//
//  RPAddCommentViewController.m
//  DrinkNDrive
//
//  Created by Ruslan on 4/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAddCommentViewController.h"
#import "JJMaterialTextfield.h"

@interface RPAddCommentViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *commentTextField;
@end

@implementation RPAddCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.commentTextField.delegate = self;
    self.commentTextField.text = self.comment;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)addCommentButtonPressed:(id)sender {
    if (self.commentTextField.text.length != 0) {
        __weak typeof(self) weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            [weakSelf.delegate addComment:weakSelf.commentTextField.text];
        }];
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
