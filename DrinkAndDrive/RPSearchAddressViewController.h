//
//  RPSearchAddressViewController.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/8/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
@interface RPSearchAddressViewController : UIViewController
@property (nonatomic, weak) id<RPChangeAddressTo> addressDelegate;
@property (nonatomic, assign) BOOL isQuickOrder;
@property (nonatomic, strong) LMAddress *address;
@end
