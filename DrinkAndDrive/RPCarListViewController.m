//
//  RPCarListViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/3/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPCarListViewController.h"
#import "RPEditCarViewController.h"
#import "Helper.h"
#import "RPCarTableViewCell.h"

@interface RPCarListViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *addCarLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<RPCar*> *carsArray;
@end

@implementation RPCarListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setHidden:YES];
    [self.addCarLabel setHidden:YES];
    [self loadCars];
    // Do any additional setup after loading the view.
}

-(void)reload{
    [self loadCars];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.carsArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPCarTableViewCell *cell = (RPCarTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    RPCar *car = self.carsArray[indexPath.row];
    cell.carNumberLabel.text = car.numberCar;
    NSString *type = @"";
    for (NSDictionary *dictionary in [RPOptions sharedOptions].typecar) {
        if ([dictionary[@"id"]integerValue] == car.typeCar) {
            type = dictionary[@"name"];
        }
    }
    cell.carNameLabel.text = [NSString stringWithFormat:@"%@ (%@)", car.marka, type];
    return cell;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    RPEditCarViewController *newController =[segue destinationViewController];
    newController.delegate = self;
    if ([segue.identifier isEqualToString:@"addCar"])
    {
        newController.isEditCarMode = NO;
        
    } else {
        newController.isEditCarMode = YES;
        RPCar *car = self.carsArray[[self.tableView indexPathForSelectedRow].row];
        newController.carId = @(car.carId);
    }
    
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"addCar"]) {
        return YES;
    }
    if ([identifier isEqualToString:@"editCar"]) {
        return YES;
    }
    return NO;
}

-(void)loadCars{
    NSDictionary *params = @{@"type":@"carlist",
                             @"userid":@([RPUser sharedUser].userid),
                             @"password":[RPUser sharedUser].password};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RPServerManager sharedManager] getCarsWithParameters:params onSuccess:^(NSMutableArray <RPCar*> *cars, NSString *errorMessage) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (errorMessage!=nil) {
                [self showErrorMessage:errorMessage];
            } else {
                self.carsArray = cars;
                [self.tableView reloadData];
            }
            
            if (self.carsArray.count==0) {
                [self.addCarLabel setHidden:NO];
                [self.tableView setHidden:YES];
            } else {
                [self.addCarLabel setHidden:YES];
                [self.tableView setHidden:NO];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка"];
            self.addCarLabel.hidden=NO;
        }];
    });
}
@end
