//
//  RPCancelOrderAlertController.m
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCancelOrderAlertController.h"

@interface RPCancelOrderAlertController ()

@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;

@end

@implementation RPCancelOrderAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cancelOrderButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.cancelOrderButton.titleLabel.numberOfLines = 2;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)tapGesture{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)cancelOrderButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf.delegate cancelOrder];
    }];
}

- (IBAction)closeOrderButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
