//
//  RPCancelOrderWithRefuseAlertController.m
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCancelOrderWithRefuseAlertController.h"
#import "RPRefuseTableViewCell.h"
#import "RPOptions.h"
#import "RPOrderRefuse.h"

@interface RPCancelOrderWithRefuseAlertController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *refusesTableView;

@end

@implementation RPCancelOrderWithRefuseAlertController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    self.refusesTableView.delegate = self;
    self.refusesTableView.dataSource = self;
    [self.refusesTableView registerNib:[UINib nibWithNibName:@"RPRefuseTableViewCell" bundle:nil]  forCellReuseIdentifier:NSStringFromClass([RPRefuseTableViewCell class])];
}
- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [RPOptions sharedOptions].type_user_cancel.count;
}

- (RPRefuseTableViewCell *)tableView:(UITableView *)tableView
              cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPRefuseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPRefuseTableViewCell class]) forIndexPath:indexPath];
    cell.refuseLabel.text = [RPOptions sharedOptions].type_user_cancel[indexPath.row].name;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPOrderRefuse *selectedRefuse = [RPOptions sharedOptions].type_user_cancel[indexPath.row];
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf.delegate cancelOrderWithRefuseID:selectedRefuse.ID];
    }];
}

@end
