//
//  AppDelegate.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/CloudMessaging.h>
#import "MapViewController.h"
#import "RPUpdateLocation.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, weak) id<RPUpdateLocation> delegate;

@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;
-(void)sendTokenToServer;
@end

