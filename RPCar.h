//
//  RPCar.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/4/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPCar : NSObject
@property (nonatomic, assign) NSInteger def;
@property (nonatomic, assign) NSInteger carId;
@property (nonatomic, assign) NSInteger typeCar;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *numberCar;
@property (nonatomic, strong) NSString *marka;
@end
