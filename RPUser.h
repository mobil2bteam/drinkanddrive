//
//  RPUser.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface RPUser : NSObject
+ (RPUser*) sharedUser;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger active;
@property (nonatomic, assign) NSInteger ball;
@property (nonatomic, assign) NSInteger happyday;
@property (nonatomic, assign) NSInteger happymonth;
@property (nonatomic, assign) NSInteger userid;
-(NSDictionary*)userToDictionary;
@end
