//
//  RPUpdateLocation.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/15/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;
@protocol RPUpdateLocation <NSObject>
-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate;
@optional
-(void)updateStatus;
-(void)updateUserLocationWith:(CLLocation*)location;
@end
