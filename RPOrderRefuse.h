//
//  RPOrderRefuse.h
//  DrinkNDrive
//
//  Created by Ruslan on 9/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPOrderRefuse : NSObject
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) NSInteger active;
@property (nonatomic, strong) NSString *name;
@end
