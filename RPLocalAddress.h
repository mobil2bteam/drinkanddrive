//
//  RPLocalAddress.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/12/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface RPLocalAddress : UIViewController
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *house;
@property (nonatomic, strong) NSString *korpus;
@property (nonatomic, strong) NSString *stroenie;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, assign) CLLocationCoordinate2D location;
@end
