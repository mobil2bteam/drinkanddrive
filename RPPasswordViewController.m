//
//  RPPasswordViewController.m
//  DrinkAndDrive
//
//  Created by Ruslan on 1/3/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPasswordViewController.h"
#import "Helper.h"
#import "RPRememberPasswordViewController.h"

@interface RPPasswordViewController()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *passwordTextField;
@end

@implementation RPPasswordViewController
#pragma mark - UITextField Delegate


-(void)viewDidLoad{
    [super viewDidLoad];
    [self addLogoToNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.passwordTextField.text = @"";
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    
    if (textField.text.length==0) {
        [textField showError];
    }else{
        if (![[textField.text md5] isEqualToString:[RPUser sharedUser].password]) {
            [self showErrorMessage:@"Неправильный пароль"];
        }
        [textField hideError];
    }
}

-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"passwordSegue"]) {
        if ([[self.passwordTextField.text md5] isEqualToString:[RPUser sharedUser].password]) {
            return  YES;
        } else {
            return NO;
        }
    }
    return NO;
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (IBAction)forgotPasswordButtonPressed:(id)sender {
    RPRememberPasswordViewController *vc = [[RPRememberPasswordViewController alloc] initWithNibName:@"RPRememberPasswordViewController" bundle:nil];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
