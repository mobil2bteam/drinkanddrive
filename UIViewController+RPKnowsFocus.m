//
//  UIViewController+RPKnowsFocus.m
//  DrinkAndDrive
//
//  Created by Ruslan on 1/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+RPKnowsFocus.h"
#import "Reachability.h"
#import "SCLAlertView.h"

@implementation UIViewController (RPKnowsFocus)
-(void) willGetFocus {
    
}

-(BOOL)isReachility{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

-(void)showErrorMessage:(NSString*)errorMessage{
    CGFloat k = 0.7;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        k = 0.3;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * k];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert showError:self title:nil
            subTitle:errorMessage
    closeButtonTitle:@"OK" duration:0.0f];
}

-(void)showWarningMessage:(NSString*)errorMessage{
    CGFloat k = 0.7;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        k = 0.3;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * k];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert showWarning:self title:nil
              subTitle:errorMessage
      closeButtonTitle:@"OK" duration:0.0f];
}

-(void)showSuccessMessage:(NSString*)message{
    CGFloat k = 0.7;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        k = 0.3;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * k];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert addButton:@"OK" actionBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert showSuccess:self title:nil subTitle:message closeButtonTitle:nil duration:0.0f];
}

-(void)addLogoToNavigationBar{
    if (self.navigationController==nil) {
        return;
    }
    UIView *logoView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2.0-50, 5.5, 100, 33)];
    [logoView setBackgroundColor:[UIColor clearColor]];
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:logoView.bounds];
    logoImageView.image = [UIImage imageNamed:@"Logo"];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [logoView addSubview:logoImageView];
    [self.navigationController.navigationBar addSubview:logoView];
}

-(void)addLeftBarButton{
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                                     initWithTitle:@"Отменить"
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(closeController)];
    self.navigationItem.leftBarButtonItem = cancelButton;
}
-(void)closeController{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
