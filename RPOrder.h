//
//  RPOrder.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPDriver.h"
#import "RPLocalAddress.h"
@interface RPOrder : NSObject
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *adddressA;
@property (nonatomic, strong) NSString *adddressB;
@property (nonatomic, assign) NSInteger numberOrder;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString *statusText;
@property (nonatomic, strong) NSString *messageText1;
@property (nonatomic, strong) NSString *messageText2;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) RPDriver *driver;
@property (nonatomic, strong) RPLocalAddress *wayFrom;
@property (nonatomic, strong) RPLocalAddress *wayTo;
+ (RPOrder*) sharedOrder;
@end
