//
//  RPOrderFull.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPDriver.h"
@interface RPOrderFull : NSObject
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *adddressA;
@property (nonatomic, strong) NSString *adddressB;
@property (nonatomic, assign) NSInteger ocenka;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, assign) NSInteger numberOrder;
@property (nonatomic, assign) NSInteger itog;
@property (nonatomic, assign) NSInteger summ;
@property (nonatomic, assign) NSInteger summBall;
@property (nonatomic, assign) NSInteger addBall;
@property (nonatomic, strong) NSString *marka;
@property (nonatomic, strong) NSString *numberCar;
@property (nonatomic, strong) NSString *dateCreate;
@property (nonatomic, strong) NSString *dateFinish;
@property (nonatomic, strong) RPDriver *driver;
@property (nonatomic, strong) NSMutableArray *wayList;

@end
