//
//  RPOrder.m
//  DrinkAndDrive
//
//  Created by Ruslan on 12/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPOrder.h"

@implementation RPOrder
+ (RPOrder*) sharedOrder {
    static RPOrder* order = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        order = [[RPOrder alloc] init];
    });
    return order;
}

@end
